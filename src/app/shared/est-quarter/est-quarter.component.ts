import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';

interface Stats {
  field: string;
  value: number;
  avgEst: number;
  lowEst: number;
  highEst: number;
}

@Component({
  selector: 'app-est-quarter',
  standalone: true,
  imports: [CommonModule, MatTableModule, MatCardModule],
  templateUrl: './est-quarter.component.html',
  styleUrl: './est-quarter.component.scss'
})
export class EstQuarterComponent {
  @Input() stats: Stats[];
  displayedColumns: string[] = ['field', 'value', 'avgEst', 'lowEst', 'highEst'];
}
