import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EstQuarterComponent } from './est-quarter.component';

describe('EstQuarterComponent', () => {
  let component: EstQuarterComponent;
  let fixture: ComponentFixture<EstQuarterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [EstQuarterComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EstQuarterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
