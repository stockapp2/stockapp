import { CommonModule, NgFor } from '@angular/common';
import { Component, Input } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { getNestedValue } from 'src/app/common/utils';

interface Stats {
  period: string;
  avg: number;
  max: number;
  min: number;
  percentageDiff?: number;
}

@Component({
  selector: 'app-stats',
  standalone: true,
  imports: [CommonModule, MatTableModule, MatCardModule],
  templateUrl: './stats.component.html',
  styleUrl: './stats.component.scss'
})
export class StatsComponent {

  @Input() field: string;
  @Input() set data(d) {
    if(d) {
      const stats = getNestedValue(d, this.field); 
      this.value = stats.value;
      const fields = Object.keys(stats);
      this.stats = fields.filter(f => stats[f]?.avg).map(f => {
        const stat = stats[f];
        return {
          period: f,
          avg: stat.avg,
          max: stat.max,
          min: stat.min,
          percentageDiff: ((this.value - stat.avg) / stat.avg) * 100
        }
      });
    }
  }

  displayedColumns: string[] = ['period', 'avg', 'max', 'min', 'percentageDiff'];

  value = 16.6;
  stats: Stats[] = [];
}
