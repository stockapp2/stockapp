export interface Daily {
  date?: string;
  open?: number;
  high?: number;
  low?: number;
  close?: number;
  adjustedClose?: number;
  adjustedCloseSplit?: number;
  volume?: number;
  dividend?: number;
  marketCap?: number;
  ev?: number;
  evData?: any;
}
