import { GraphPoint } from "./graph-point.model";

export interface GraphInfo {
    field: string;
    label: string;
    points: GraphPoint[];
    color: string;
  }