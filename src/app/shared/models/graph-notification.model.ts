export enum TypeField {
  QUARTER,
  DAILY
}


export interface GraphNotification {
    symbol: string;
    fieldsSelected: string[];
    typeField: TypeField
  }