import { FieldSetting } from "./field-setting.model";

export interface ColumnsSetting {
  id?: string;
  name?: string;
  fields?: FieldSetting[];
  type?: string;
}