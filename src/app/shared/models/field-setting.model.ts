export interface FieldSetting {
  field?: string;
  valueMax?: number;
  valueMin?: number;
  comparator?: 'MORE_THAN' | 'LESS_THAN' | 'BETWEEN' | 'EQUAL';
}