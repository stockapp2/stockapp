export interface RowInfo {
    id?: string;
    parentId?: string;
    mainParentId?: string;
    field?: string;
    values: any[];
    hide?: boolean;
    canExpand?: boolean;
  }