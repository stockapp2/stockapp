import { QuarterDataStats } from "./quarter-data-stats.model";

export interface QuarterData {
    value?: number;
    growth?: number;
    perShare?: number;
    perShareGrowth?: number;
    ltm?: number;
    ltmGrowth?: number;
    ltmPerShare?: number;
    ltmPerShareGrowth?: number;
    fullStats?: QuarterDataStats;
    lastYStats?: QuarterDataStats;
    last3yStats?: QuarterDataStats;
    last5yStats?: QuarterDataStats;
}