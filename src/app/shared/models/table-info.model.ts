import { HeaderRowInfo } from "./header-row-info.model";
import { RowInfo } from "./row-info.model";

export interface TableInfo {
    symbol: string;
    headerInfoList: HeaderRowInfo[];
    bodyInfoList: RowInfo[];
  }