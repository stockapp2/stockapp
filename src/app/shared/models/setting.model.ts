import { ColumnsSetting } from "./columns-setting.model";

export interface Setting {
  viewSettings?: ColumnsSetting[];
  filterSettings?: ColumnsSetting[];
}
