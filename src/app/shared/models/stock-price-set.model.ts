export interface StockPriceSet {
  date?: string;
  prices?: { [key: string]: number }
}
