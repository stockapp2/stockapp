export interface QuarterDataStats {
    growthAvg?: number;
    perShareGrowthAvg?: number;
    ltmGrowthAvg?: number;
    ltmPerShareGrowthAvg?: number;
}