import { Daily } from "./price.model";
import { Quarter } from "./quarter.model";

export interface Stock {
    id?: string;
    symbol?: string;
    currency?: string;
    exchange?: string;
    country?: string;
    sector?: string;
    industry?: string;
    prices?: Daily[],
    quarters?: Quarter[],
    lastQuarter?: Quarter,
    lastPrice?: Daily,
    actualQuarter?: Quarter,
    actualPrice?: Daily
}