import { EntityUpdateType } from '../enums/entity-update-type.enum';
import { Entity } from '../enums/entity.enum';

export class EntityUpdateEvent {

  constructor(public entity: Entity, public type: EntityUpdateType, public id: string, public payload: any) {
  }

}
