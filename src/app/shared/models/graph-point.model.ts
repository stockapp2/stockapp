import { DateTime } from "luxon";

export interface GraphPoint {
    date: DateTime;
    value: number;
  }