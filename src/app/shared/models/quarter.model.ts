import { QuarterData } from "./quarter-data.model";

export interface Quarter {
    earningDate?: string;
    fiscalDateEnd?: string;
    // Income Statement
    revenues?: number;
    costsOfGoodsSold?: number;
    grossProfit?: number;
    operatingExpenses?: number;
    operatingIncome?: number;
    netIncome?: number;
    shares?: number;
    // Balance Sheet
    totalCashAndShortInv?: number;
    totalReceivable?: number;
    totalCurrentAssets?: number;
    netPropertyPlantAndEquip?: number;
    goodwill?: number;
    totalAssets?: number;
    totalCurrentLiabilities?: number;
    longTermDebt?: number;
    totalLiabilities?: number;
    totalEquity?: number;
    netDebt?: number;
    // Cash Flow Statement
    totalDeprecAmort?: number;
    stockBasedCompensation?: number;
    cashFromOperations?: number;
    capitalExpenditure?: number;
    cashFromInvesting?: number;
    cashFromFinancing?: number;
    netChangeinCash?: number;
    // Calculated
    eps?: number;
    ebit?: number;
    ebitda?: number;
    freeCashFlow?: number;
    currentRatio?: number;
    roi?: number;
    roe?: number;
    roa?: number;
    // Data
    revenuesData?: QuarterData;
    grossProfitData?: QuarterData;
    operatingIncomeData?: QuarterData;
    netIncomeData?: QuarterData;
    epsData?: QuarterData;
    ebitdaData?: QuarterData;
    freeCashFlowData?: QuarterData;
  }