import { CommonModule, NgFor } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { ColumnsSetting } from '../models/columns-setting.model';
import { cloneDeep, first, isEqual, uniqueId } from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import { MatDialog } from '@angular/material/dialog';
import { ColumnSelectorDialogComponent } from '../column-selector-dialog/column-selector-dialog.component';

@Component({
  selector: 'app-setting-selector',
  standalone: true,
  imports: [MatIconModule, NgFor, CommonModule],
  templateUrl: './setting-selector.component.html',
  styleUrl: './setting-selector.component.scss'
})
export class SettingSelectorComponent {
  @Input() title;
  @Input() selectorType = 'view';
  @Input() set columnsSetting(columns: ColumnsSetting[]) {
    this.columns = columns;
    if (!this.columnSelected) {
      this.onColumnClick(first(this.columns));
    }
  };

  @Output() columnSelectedEmitter = new EventEmitter<ColumnsSetting>;
  @Output() columsEmitter = new EventEmitter<ColumnsSetting[]>;

  public columns: ColumnsSetting[];
  public columnSelected: ColumnsSetting;

  constructor(public dialog: MatDialog) {}

  public onColumnClick(column: ColumnsSetting) {
    if(this.columnSelected?.id === column?.id) {
      this.columnSelected = null;
    } else {
      this.columnSelected = column;
    }
    this.columnSelectedEmitter.emit(this.columnSelected);
  }

  openFieldsSelector(column: ColumnsSetting) {
    this.dialog.open(ColumnSelectorDialogComponent, {
      data: cloneDeep(column  || {id: uuidv4(), name: '', type: this.selectorType, fields: []} as ColumnsSetting),
      width: '1000px'
    }).afterClosed().subscribe((result: ColumnsSetting) => {
      if (result) {
        this.columns = this.updateCacheElement(this.columns, result);
        this.onColumnClick(result);
        this.columsEmitter.emit(this.columns);
      }
    });
  }

  public updateCacheElement(list, element) {
    const index: number = list.findIndex((e) => e.id === element.id);
    if (index !== -1) {
      const eFound = this.columns[index];
      if (!isEqual(eFound, element)) {
        list.splice(index, 1, element);
      }
    } else {
      list.push(element);
    }
    return list;
  }

  public removeFromCache(list: any[], id: string) {
    const index: number = list.findIndex(e => e.id === id);
    if (index !== -1) {
      list.splice(index, 1);
    }
    return list;
  }

  onDeleteColumnClick(column: ColumnsSetting) {
    this.columns = this.removeFromCache(this.columns, column.id);
    this.columsEmitter.emit(this.columns);
  }

}
