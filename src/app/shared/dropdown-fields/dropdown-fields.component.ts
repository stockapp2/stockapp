import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TypeField } from '../models/graph-notification.model';
import { NotificationService } from '../services/notification.service';
import { MatOption } from '@angular/material/core';
import { NgFor } from '@angular/common';
import { MatSelect } from '@angular/material/select';
import { MatFormField, MatLabel } from '@angular/material/form-field';

@Component({
    selector: 'app-dropdown-fields',
    templateUrl: './dropdown-fields.component.html',
    styleUrls: ['./dropdown-fields.component.css'],
    standalone: true,
    imports: [MatFormField, MatLabel, MatSelect, FormsModule, ReactiveFormsModule, NgFor, MatOption]
})
export class DropdownFieldsComponent implements OnInit {

  public fieldsControl = new UntypedFormControl('');
  public obj: any;
  public fields: string[];

  @Input() stockSymbol: string;

  @Input() set object(obj: any) {
    this.obj = obj;
    if(obj) {
      this.fields = Object.keys(obj);
    }
  };

  @Output() fieldsSelected = new EventEmitter<string[]>();

  constructor() { }

  ngOnInit(): void {
  }

  onSelectionChange(event) {
    this.fieldsSelected.emit(event.value);
  }

}
