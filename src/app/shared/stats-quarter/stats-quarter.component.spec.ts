import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatsQuarterComponent } from './stats-quarter.component';

describe('StatsQuarterComponent', () => {
  let component: StatsQuarterComponent;
  let fixture: ComponentFixture<StatsQuarterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StatsQuarterComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StatsQuarterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
