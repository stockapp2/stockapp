import { Component, Input } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';

@Component({
  selector: 'app-stats-quarter',
  standalone: true,
  imports: [MatTableModule, MatCardModule],
  templateUrl: './stats-quarter.component.html',
  styleUrl: './stats-quarter.component.scss'
})
export class StatsQuarterComponent {

  public data;

  @Input() field: string;
  @Input() set dataQuarter(dataQuarter: any) {
    this.data = dataQuarter[this.field];
  }

  displayedColumns: string[] = ['metric', 'growth', 'perShareGrowth', 'ltmGrowth', 'ltmPerShareGrowth'];
  displayedColumnsAvg: string[] = ['period', 'growthAvg', 'perShareGrowthAvg', 'ltmGrowthAvg', 'ltmPerShareGrowthAvg'];

  get fullStats() {
    return [this.data.lastYStats, this.data.last3yStats, this.data.last5yStats, this.data.fullStats];
  }
}
