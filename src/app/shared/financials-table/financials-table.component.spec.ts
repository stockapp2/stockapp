import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancialsTableComponent } from './financials-table.component';

describe('FinancialsTableComponent', () => {
  let component: FinancialsTableComponent;
  let fixture: ComponentFixture<FinancialsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
    imports: [FinancialsTableComponent]
})
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
