import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { flattenDeep } from 'lodash';
import { avgFields, fieldRanges, futureRanges } from 'src/app/common/utils';
import { Quarter } from '../models/quarter.model';
import { RowInfo } from '../models/row-info.model';
import { Stock } from '../models/stock.model';
import { TableInfo } from '../models/table-info.model';
import { ExpandableTableComponent } from '../expandable-table/expandable-table.component';
import { MatTabGroup, MatTab } from '@angular/material/tabs';

export interface FieldInfo {
  field: string;
  showRelated?: boolean;
}

@Component({
    selector: 'app-financials-table',
    templateUrl: './financials-table.component.html',
    styleUrls: ['./financials-table.component.scss'],
    standalone: true,
    imports: [MatTabGroup, MatTab, ExpandableTableComponent]
})
export class FinancialsTableComponent implements OnInit {

  @Output() fieldsSelected = new EventEmitter<string[]>();

  @Input() set stockData(stock: Stock) {
    this.stock = stock;
    if(this.stock?.quarters) {
      this.incomeInfo = this.calculateTableInfo([
        {field: 'revenues', showRelated: true},
        {field: 'costsOfGoodsSold', showRelated: false},
        {field: 'grossProfit', showRelated: true},
        {field: 'operatingExpenses', showRelated: false},
        {field: 'operatingIncome', showRelated: true},
        {field: 'netIncome', showRelated: true},
        {field: 'shares', showRelated: true}
      ]);

      this.balanceInfo = this.calculateTableInfo([
        {field: 'totalCashAndShortInv', showRelated: false},
        {field: 'totalReceivable', showRelated: false},
        {field: 'totalCurrentAssets', showRelated: false},
        {field: 'netPropertyPlantAndEquip', showRelated: false},
        {field: 'goodwill', showRelated: false},
        {field: 'totalAssets', showRelated: false},
        {field: 'totalCurrentLiabilities', showRelated: false},
        {field: 'longTermDebt', showRelated: false},
        {field: 'totalLiabilities', showRelated: false},
        {field: 'totalEquity', showRelated: false},
        {field: 'netDebt', showRelated: false}
      ]);

      this.cashFlowInfo = this.calculateTableInfo([
        {field: 'totalDeprecAmort', showRelated: false},
        {field: 'stockBasedCompensation', showRelated: false},
        {field: 'cashFromOperations', showRelated: false},
        {field: 'capitalExpenditure', showRelated: false},
        {field: 'cashFromInvesting', showRelated: false},
        {field: 'cashFromFinancing', showRelated: false},
        {field: 'netChangeinCash', showRelated: false}
      ]);

      this.ratiosInfo = this.calculateTableInfo([
        {field: 'freeCashFlow', showRelated: true},
        {field: 'eps', showRelated: true},
        {field: 'ebit', showRelated: true},
        {field: 'ebitda', showRelated: true},
        {field: 'roi', showRelated: false},
        {field: 'roe', showRelated: false},
        {field: 'roa', showRelated: false},
        {field: 'currentRatio', showRelated: false}
      ]);
    }
  }

  public stock: Stock;
  public incomeInfo: TableInfo;
  public balanceInfo: TableInfo;
  public cashFlowInfo: TableInfo;
  public ratiosInfo: TableInfo;

  public incomeFieldsSelected: string[] = [];
  public balanceFieldsSelected: string[] = [];
  public cashFlowFieldsSelected: string[] = [];
  public ratiosFieldsSelected: string[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  public calculateTableInfo(values: FieldInfo[]): TableInfo {
    const quarters: Quarter[] = this.stock.quarters;
    let tableInfo = {
      symbol: this.stock.symbol,
      headerInfoList: [{}, {}],
      bodyInfoList: []
    } as TableInfo;
    tableInfo.headerInfoList[0].values = ['Fiscal Date'].concat(quarters.map(q => q.fiscalDateEnd));
    tableInfo.headerInfoList[1].values = ['Earnings Date'].concat(quarters.map(q => q.earningDate));
    let rows: RowInfo[] = [];
    values.forEach(v => {
      rows.push({
        values: [v.field].concat(quarters.map(q => q[v.field])),
        id: v.field,
        field: v.field
      });
      if(v.showRelated) {
        this.getTableRowsNames(v.field).forEach(f => {
          rows.push({
            values: [f].concat(quarters.map(q => q[f])),
            id: f,
            parentId: v.field,
            mainParentId: v.field,
            field: f
          });
          if(f.includes('Growth')) {
            this.getTableRowsRangeNames(f).forEach(fr => {
              rows.push({
                values: [fr].concat(quarters.map(q => q[fr])),
                parentId: f,
                mainParentId: v.field,
                field: fr
              });
            });
          } else if (!f.includes('Margin')) {
            this.getTableRowsEstimatedNames(f).forEach(fr => {
              rows.push({
                values: [fr].concat(quarters.map(q => q[fr])),
                parentId: f,
                mainParentId: v.field,
                field: fr
              });
            });
          }
        });
        rows.push({
          values: [v.field + 'EstNextQMedian'].concat(quarters.map(q => q[v.field + 'EstNextQMedian'])),
          id: v.field + 'Est',
          parentId: v.field,
          mainParentId: v.field,
          field: v.field + 'Est'
        });
        this.getTableRowsEstimatedNames(v.field).forEach(fr => {
          rows.push({
            values: [fr].concat(quarters.map(q => q[fr])),
            parentId: v.field + 'Est',
            mainParentId: v.field,
            field: fr
          });
        });
      }
    });
    tableInfo.bodyInfoList = rows;
    return tableInfo;
  }

  public getTableRowsNames(field: string): string[] {
    let margin = field + 'Margin';
    let growth = field + 'Growth';
    let ltmField = field + 'Ltm';
    let ltmGrowthField = field + 'LtmGrowth';
    let ltmPerShareField = field + 'LtmPerShare';
    let ltmPerShareGrowthField = field + 'LtmPerShareGrowth';
    let fields = [margin, growth, ltmField, ltmGrowthField, ltmPerShareField, ltmPerShareGrowthField];
    return fields;
  }

  public getTableRowsRangeNames(field: string): string[] {
    let fields = [];
    fields.push(field + 'Min');
    fields.push(field + 'Median');
    fields.push(field + 'Max');
    fields.push(field + 'Std' + 'Min');
    fields.push(field + 'Std' + 'Median');
    fields.push(field + 'Std' + 'Max');
    let baseFields = flattenDeep(fieldRanges.map(f => avgFields.map(favg => field + f + favg)));
    fields = fields.concat(baseFields);
    fields = fields.concat(baseFields.map(f => f + 'Std'));
/*     fieldRanges.forEach(f => {
      avgFields.forEach(favg => {
        fields.push(field + f + favg);
        fields.push(field + f + favg + 'Std') ;
      });
    }); */
    return fields;
  }

  public getTableRowsEstimatedNames(field: string): string[] {
    let fields = [];
    futureRanges.forEach(fnext => {
      fields.push(field + 'Est' + 'Next' + fnext + 'Min');
      fields.push(field + 'Est' + 'Next' + fnext + 'Median');
      fields.push(field + 'Est' + 'Next' + fnext + 'Max');
      fieldRanges.forEach(f => {
        avgFields.forEach(favg => {
          fields.push(field + 'Est' + f + favg + 'Next' + fnext);
        });
      });
    });
    return fields;
  }

  public onIncomeFieldsSelected(fields: string[]) {
    this.incomeFieldsSelected = fields;
    this.sendFielsSelected();
  }

  public onBalanceFieldsSelected(fields: string[]) {
    this.balanceFieldsSelected = fields;
    this.sendFielsSelected();
  }

  public onCashFlowFieldsSelected(fields: string[]) {
    this.cashFlowFieldsSelected = fields;
    this.sendFielsSelected();
  }

  public onRatiosFieldsSelected(fields: string[]) {
    this.ratiosFieldsSelected = fields;
    this.sendFielsSelected();
  }

  public sendFielsSelected() {
    const fieldsSelected = this.incomeFieldsSelected.concat(this.balanceFieldsSelected, this.cashFlowFieldsSelected, this.ratiosFieldsSelected);
    this.fieldsSelected.emit(fieldsSelected);
  }

}
