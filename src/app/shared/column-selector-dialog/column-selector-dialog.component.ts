import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialogTitle, MatDialogContent, MatDialogActions, MatDialogClose } from '@angular/material/dialog';
import { ColumnsSetting } from '../models/columns-setting.model';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StockService } from '../services/stock.service';
import { MatButton } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';
import { MatSelect } from '@angular/material/select';
import { MatOption } from '@angular/material/core';
import { NgFor, NgIf } from '@angular/common';
import { MatAutocompleteTrigger, MatAutocomplete } from '@angular/material/autocomplete';
import { MatInput } from '@angular/material/input';
import { MatFormField, MatLabel } from '@angular/material/form-field';
import { CdkScrollable } from '@angular/cdk/scrolling';
import { FieldSetting } from '../models/field-setting.model';

@Component({
    selector: 'app-column-selector-dialog',
    templateUrl: './column-selector-dialog.component.html',
    styleUrls: ['./column-selector-dialog.component.scss'],
    standalone: true,
    imports: [MatDialogTitle, CdkScrollable, MatDialogContent, FormsModule, MatFormField, MatInput, MatAutocompleteTrigger, ReactiveFormsModule, MatAutocomplete, NgFor, MatOption, NgIf, MatLabel, MatSelect, MatIcon, MatDialogActions, MatButton, MatDialogClose]
})
export class ColumnSelectorDialogComponent implements OnInit {

  public view: ColumnsSetting;
  public options: string[] = [];
  public columns: FieldSetting[] = [];
  filteredOptions: string[];
  comparatorOptions = ['more than' ,'less than' , 'between' , 'equal'];
  myControl = new FormControl('');
  @ViewChild('input') input: ElementRef<HTMLInputElement>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: ColumnsSetting, private dialogRef: MatDialogRef<ColumnSelectorDialogComponent>, private stockService: StockService) {}

  ngOnInit(): void {
    this.options = this.stockService.getStockFields();
    this.view = this.data;
    this.columns = this.view.fields;
  }

  public onOptionClicked(option: string) {
    const valueSelected = this.myControl.value;
    if (valueSelected) {
      this.addColumn({field: valueSelected});
      this.clearSearch();
    }
  }

  public clearSearch() {
    this.filteredOptions = this.options;
    this.myControl.setValue('');
  }

  public filter(): void {
    const filterValue = this.input.nativeElement.value.toLowerCase();
    this.filteredOptions = this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  public onDeleteColumnClick(c: FieldSetting) {
    this.removeColumn(c);
  }

  addColumn(c: FieldSetting): void {
    if (!this.columns.some(existingColumn => existingColumn.field === c.field)) {
      this.columns.push(c);
    }
  }

  removeColumn(c: FieldSetting): void {
    this.columns = this.columns.filter(existingColumn => existingColumn.field !== c.field);
  }

  moveUp(index: number): void {
    if (index > 0) {
      [this.columns[index - 1], this.columns[index]] = [this.columns[index], this.columns[index - 1]];
    }
  }

  moveDown(index: number): void {
    if (index < this.columns.length - 1) {
      [this.columns[index], this.columns[index + 1]] = [this.columns[index + 1], this.columns[index]];
    }
  }

  onDialogClose() {
    this.view.fields = this.columns;
    this.dialogRef.close(this.view);
  }
}
