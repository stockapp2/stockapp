import { Component, Input, OnInit } from '@angular/core';
import { Stock } from '../models/stock.model';
import { MatTooltip } from '@angular/material/tooltip';
import { NgFor } from '@angular/common';

export interface WatchlistView {
  values: string[];
}

@Component({
    selector: 'app-watchlist-table',
    templateUrl: './watchlist-table.component.html',
    styleUrls: ['./watchlist-table.component.scss'],
    standalone: true,
    imports: [NgFor, MatTooltip]
})
export class WatchlistTableComponent implements OnInit {

  public stocksInfo: Stock[] = [];
  public fields: string[] = [];
  public view: WatchlistView[] = [];

  @Input() set fieldsSelected(fields: any[]) {
    this.fields = fields.map(f => f['item_text']);
    this.generateView();
  }

  @Input() set stocks(stocksInfo: Stock[]) {
    this.stocksInfo = stocksInfo;
    this.generateView();
  }

  constructor() { }

  ngOnInit(): void {
  }

  public generateView() {
    const stocksFlatted = this.stocksInfo?.map(s => {
      let merged = {...s, ...s.actualQuarter, ...s.actualPrice};
      return merged;
    });
    this.view = stocksFlatted?.map(s => this.getViewFromStockFlat(s));
  }

  public getViewFromStockFlat(stock: any): WatchlistView {
    return {
      values: this.fields.map(f => stock[f])
    }
  }

}
