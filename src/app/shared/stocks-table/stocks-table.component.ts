import { Component, Input } from '@angular/core';
import { Stock } from '../models/stock.model';
import { CommonModule, NgFor, NgIf } from '@angular/common';

@Component({
    selector: 'app-stocks-table',
    templateUrl: './stocks-table.component.html',
    styleUrls: ['./stocks-table.component.scss'],
    standalone: true,
    imports: [NgFor, NgIf, CommonModule]
})
export class StocksTableComponent {
  @Input() fields: string[] = [];
  @Input() stocks: Stock[] = [];

  sortField: string = '';
  sortOrder: string = 'asc';

  getNestedValue(model: Stock, fieldPath: string): any {
    return fieldPath.split('.').reduce((obj, key) => obj && obj[key], model);
  }

  formatField(field: string) {
    return field.replace('actualQuarter', 'aQ').replace('Data', '').replace('Stats', '');
  }

  sortBy(field: string): void {
    if (this.sortField === field) {
      this.sortOrder = this.sortOrder === 'asc' ? 'desc' : 'asc';
    } else {
      this.sortField = field;
      this.sortOrder = 'asc';
    }

    this.stocks.sort((a, b) => {
      const valueA = this.getNestedValue(a, field);
      const valueB = this.getNestedValue(b, field);

      let comparison = 0;
      if (valueA > valueB) {
        comparison = 1;
      } else if (valueA < valueB) {
        comparison = -1;
      }

      return this.sortOrder === 'asc' ? comparison : -comparison;
    });
  }
}
