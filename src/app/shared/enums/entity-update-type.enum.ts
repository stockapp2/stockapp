export enum EntityUpdateType {
    CREATED = 'CREATED',
    UPDATED = 'UPDATED',
    DELETED = 'DELETED',
    REJECTED = 'REJECTED'
  }