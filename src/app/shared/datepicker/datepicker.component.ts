import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatDatepickerInputEvent, MatDatepickerInput, MatDatepickerToggle, MatDatepicker } from '@angular/material/datepicker';
import { DateTime } from 'luxon';
import { MatInput } from '@angular/material/input';
import { MatFormField, MatLabel, MatSuffix } from '@angular/material/form-field';
import { TimeService } from '../services/time.service';
import { FormsModule } from '@angular/forms';

@Component({
    selector: 'app-datepicker',
    templateUrl: './datepicker.component.html',
    styleUrls: ['./datepicker.component.scss'],
    standalone: true,
    imports: [MatFormField, MatLabel, MatInput, MatDatepickerInput, MatDatepickerToggle, MatSuffix, MatDatepicker, FormsModule]
})
export class DatepickerComponent implements OnInit {

  public selectedDate: Date;

  constructor(private dateService: TimeService) {

  }

  ngOnInit(): void {
    this.selectedDate = TimeService.dateNow.toJSDate();
  }

  dateChange(event: MatDatepickerInputEvent<Date>) {
    this.selectedDate = event.value;
    this.dateService.setActualDate(DateTime.fromJSDate(this.selectedDate));
  }
}
