import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { uniqBy } from 'lodash';
import { Subscription } from 'rxjs';
import { GraphNotification, TypeField } from '../models/graph-notification.model';
import { Stock } from '../models/stock.model';
import { GraphService } from '../services/graph.service';
import { NotificationService } from '../services/notification.service';
import { StockService } from '../services/stock.service';
import { MatButton } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { MatInput } from '@angular/material/input';

@Component({
    selector: 'app-graph-stock',
    templateUrl: './graph-stock.component.html',
    styleUrls: ['./graph-stock.component.scss'],
    encapsulation: ViewEncapsulation.None,
    standalone: true,
    imports: [MatInput, FormsModule, MatButton]
})
export class GraphStockComponent implements OnInit, OnDestroy {

  private subscription: Subscription;

  public minScaleNumber;
  public maxScaleNumber;

  private stocks: Stock[] = [];
  private quarterFields: string[] = [];
  private dailyFields: string[] = [];

  @ViewChild('graphDiv', { static: true }) graphDiv: ElementRef;

  constructor(private notificationService: NotificationService, private stockService: StockService, private graphService: GraphService) { }

  ngOnInit(): void {
    this.subscription = this.notificationService.showGraph$.subscribe((n: GraphNotification) => {
      this.stocks.push(this.stockService.findBySymbol(n.symbol));
      this.stocks = uniqBy(this.stocks, 'symbol');
      if(n.typeField === TypeField.QUARTER) {
        this.quarterFields = n.fieldsSelected;
      } else {
        this.dailyFields = n.fieldsSelected;
      }
      this.drawGraph();
    });
  }

  private drawGraph() {
    this.graphService.drawGraph(this.stocks, this.quarterFields, this.dailyFields, this.graphService.getDimension(this.graphDiv));
  }

  applyScales() {
    this.graphService.updateYScale(this.minScaleNumber, this.maxScaleNumber);
  }

  reset() {
    this.quarterFields = [];
    this.dailyFields = [];
    this.drawGraph();
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

}
