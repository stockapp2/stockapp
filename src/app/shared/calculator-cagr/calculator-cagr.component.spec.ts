import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculatorCagrComponent } from './calculator-cagr.component';

describe('CalculatorCagrComponent', () => {
  let component: CalculatorCagrComponent;
  let fixture: ComponentFixture<CalculatorCagrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CalculatorCagrComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CalculatorCagrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
