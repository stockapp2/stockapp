import { NgClass, NgIf } from '@angular/common';
import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatCard } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { applyCagr, calculateCagr, getPercentage } from 'src/app/common/math-utils';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-calculator-cagr',
  standalone: true,
  imports: [MatCard, FormsModule, MatFormFieldModule, MatInputModule, NgClass, MatSlideToggleModule, NgIf],
  templateUrl: './calculator-cagr.component.html',
  styleUrl: './calculator-cagr.component.scss'
})
export class CalculatorCagrComponent {

  public initialValue: number = 270;
  public yearsToProject: number = 3;
  public cagr: number;
  public endingValue: number;
  
  public endingValueResult: number;
  public cagrResult: number;
  public growth: number;

  public cagrMode: boolean = true;

  public updateCalculation() {
    if (this.cagrMode && this.initialValue && this.endingValue && this.yearsToProject) {
      this.cagrResult = calculateCagr(this.initialValue, this.endingValue, this.yearsToProject);
      this.growth = getPercentage(this.endingValue, this.initialValue);
    }
    if (!this.cagrMode && this.initialValue && this.yearsToProject && this.cagr) {
      this.endingValueResult = applyCagr(this.initialValue, this.cagr, this.yearsToProject);
      this.growth = getPercentage(this.endingValueResult, this.initialValue);
    }
  }

  onToggleChange() {
    this.updateCalculation();
  }
}
