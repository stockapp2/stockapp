import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { TypeField } from '../models/graph-notification.model';
import { RowInfo } from '../models/row-info.model';
import { TableInfo } from '../models/table-info.model';
import { NotificationService } from '../services/notification.service';
import { MatIcon } from '@angular/material/icon';
import { NgFor, NgIf, NgClass } from '@angular/common';

@Component({
    selector: 'app-expandable-table',
    templateUrl: './expandable-table.component.html',
    styleUrls: ['./expandable-table.component.scss'],
    standalone: true,
    imports: [NgFor, NgIf, MatIcon, NgClass]
})
export class ExpandableTableComponent implements OnInit {

  public tableInfo: TableInfo;

  public fieldsSel: string[] = [];

  @Input() set info(tableInfo: TableInfo) {
    this.tableInfo = tableInfo;
    this.tableInfo?.bodyInfoList?.map(r =>{
      if (r.mainParentId) {
        r.hide = true;
      }
      if (r.id && this.tableInfo.bodyInfoList.find(r1 => r1.parentId === r.id)) {
        r.canExpand = true;
      }
      return r;
    });
  };

  @Output() fieldsSelected = new EventEmitter<string[]>();

  constructor(private notificationService: NotificationService) { }

  ngOnInit(): void {
    
  }

  public onExpandRowClick(row: RowInfo) {
    if(!row.mainParentId) {
      this.tableInfo.bodyInfoList.filter(r => r.id && r.mainParentId === row.id).forEach(r => r.hide = !r.hide);
      this.tableInfo.bodyInfoList.filter(r => !r.id && r.mainParentId === row.id).forEach(r => r.hide = true);
    } else {
      this.tableInfo.bodyInfoList.filter(r => r.parentId === row.id).forEach(r => r.hide = !r.hide);
    }
  }

  public onShowGraphClick(row: RowInfo) {
    if(this.fieldsSel.find(f => f === row.field)) {
      this.fieldsSel = this.fieldsSel.filter(f => f !== row.field);
    } else {
      this.fieldsSel.push(row.field);
    }
    this.fieldsSelected.emit(this.fieldsSel);
  }

}


