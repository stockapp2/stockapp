import { Injectable } from '@angular/core';
import { isEqual } from 'lodash';
import { Subject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpService } from './http.service';

export interface Sample {
  id: string;
}

@Injectable({
  providedIn: 'root'
})
export class SampleService {

  private cache: Sample[] = [];

  private cachedSampleChange: Subject<Sample> = new Subject<Sample>();
  cachedSampleChange$: Observable<Sample> = this.cachedSampleChange.asObservable();

  constructor(private httpService: HttpService) { }

  public getSampleList(): Promise<Sample[]> {
    return this.httpService.get('sample').pipe(map((samples: Sample[]) => this.setCache(samples))).toPromise();
  }

  public updateCacheElement(sample: Sample) {
    const index: number = this.cache.findIndex((sampleC: Sample) => sampleC.id === sample.id);
    if (index !== -1) {
      const cachedSample: Sample = this.cache[index];
      if (!isEqual(cachedSample, sample)) {
        this.cache.splice(index, 1, sample);
        this.cachedSampleChange.next(sample);
      }
    } else {
      this.cache.push(sample);
      this.cachedSampleChange.next(sample);
    }
  }

  public removeFromCache(id: string) {
    const index: number = this.cache.findIndex(vesselVisit => vesselVisit.id === id);
    if (index !== -1) {
      const visit: Sample = this.cache[index];
      this.cache.splice(index, 1);
      this.cachedSampleChange.next(visit);
    }
  }

  public setCache(data: Sample[]): Sample[] {
    this.cache = data;
    return this.cache;
  }

  public getFromCache(): Sample[] {
    return JSON.parse(JSON.stringify(this.cache));
  }

}
