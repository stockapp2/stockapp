import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { GraphNotification } from '../models/graph-notification.model';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  public showGraph: Subject<GraphNotification> = new Subject<GraphNotification>();
  public showGraph$: Observable<GraphNotification> = this.showGraph.asObservable();

  constructor() { }
}
