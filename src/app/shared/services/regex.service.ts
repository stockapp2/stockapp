import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RegexService {

  public regexFiscalPeriodEnd = /([0-9]+['\/'][0-9]+['\/'][0-9]+)/g;

  public regexEarningDate = /([A-z]{3}[' '][0-9]{2}[','][' '][0-9]{4})/g;

  public regexNumber = /(['(']{0,1}[0-9]+[',','.'][0-9]+)/g;

  public regexSplitDate = /([A-z]{3}[' '][0-9]{2}[','][' '][0-9]{4})/g;

  public regexSplitRatio = /[0-9][':'][0-9]/g;

  public regexActualPrice = /[A-Z]{1,6}['\t'][0-9]{1,5}[',']{0,1}[0-9]{1,5}/g;

  constructor() {
  }

  public getActualPriceInfo(text: string): string[] {
    return this.splitRegexBasic(this.regexActualPrice, text);
  }

  public getSplitRatios(text: string): string[] {
    return this.splitRegexBasic(this.regexSplitRatio, text);
  }

  public getSplitDates(text: string): string[] {
    return this.splitRegex(this.regexSplitDate, text);
  }

  public getFiscalPeriodEnd(text: string): string[] {
    return this.splitRegex(this.regexFiscalPeriodEnd, text);
  }

  public getEarningDates(text: string): string[] {
    return this.splitRegex(this.regexEarningDate, text);
  }

  public getNumberList(text: string): string[] {
    return this.splitRegex(this.regexNumber, text);
  }

  public splitRegex(regex: RegExp, text: string): string[] {
    const info: string[] = [];
    let m;
    while ((m = regex.exec(text)) !== null) {
      // This is necessary to avoid infinite loops with zero-width matches
      if (m.index === regex.lastIndex) {
        regex.lastIndex++;
      }

      // The result can be accessed through the `m`-variable.
      m.forEach((match, groupIndex) => {
        info.push(match);
      });
    }
    return info.filter((d, i) => i % 2 === 0);
  }

  public splitRegexBasic(regex: RegExp, text: string): string[] {
    const info: string[] = [];
    let m;
    while ((m = regex.exec(text)) !== null) {
      // This is necessary to avoid infinite loops with zero-width matches
      if (m.index === regex.lastIndex) {
        regex.lastIndex++;
      }

      // The result can be accessed through the `m`-variable.
      m.forEach((match, groupIndex) => {
        info.push(match);
      });
    }
    return info;
  }
}
