import { Injectable } from '@angular/core';
import { firstValueFrom, map } from 'rxjs';
import { HttpService } from './http.service';
import { Setting } from '../models/setting.model';
import { cloneDeep, uniqueId } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class SettingService {

  public setting: Setting = {viewSettings: [{id: uniqueId(), name: 'Default', type: 'view', fields: [{field: 'symbol'}]}], filterSettings: []};

  constructor(private httpService: HttpService) { }

  public getSetting(): Promise<Setting> {
    return firstValueFrom(this.httpService.get("setting")
    .pipe(map((setting: Setting) => this.setCache(setting))))
    .catch(() => this.setCache(null));
  }

  public saveSetting(setting: Setting): Promise<Setting> {
    return firstValueFrom(this.httpService.post("setting", setting)
    .pipe(map((setting: Setting) => this.setCache(setting))))
    .catch(() => this.setCache(null));
  }

  public setCache(setting: Setting) {
    if (setting) {
      this.setting = setting;
    }
    return this.setting;
  }

  public getCache() {
    return cloneDeep(this.setting);
  }
}
