import { Injectable } from '@angular/core';
import { clone, isEqual, upperFirst } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Stock } from '../models/stock.model';
import { HttpService } from './http.service';
import { Quarter } from '../models/quarter.model';
import { applyCagr, calculateCagr, getNewValue, getPercentage, mean } from 'src/app/common/math-utils';
import { TimeService } from './time.service';
import { StockPriceSet } from '../models/stock-price-set.model';
import { Daily } from '../models/price.model';
import { getNestedValue } from 'src/app/common/utils';

@Injectable({
  providedIn: 'root'
})
export class StockService {

  private cache: Stock[] = [];
  private stockFields: string[] = [];
  private quarterFields: string[] = ["revenues"];
  private priceFields: string[] = ["revenues"];
  private quarterDataFieldFields: string[] = [];
  private quarterDataFields: string[] = [];
  private priceDataFields: string[] = [];
  private priceDataFieldFields: string[] = [];
  private quarterDataFieldsSet: string[] = [];
  private priceDataFieldsSet: string[] = [];
  private stockPriceCache: StockPriceSet;

  private cacheChange: Subject<Stock> = new Subject<Stock>();
  cacheChange$: Observable<Stock> = this.cacheChange.asObservable();

  constructor(private httpService: HttpService) { }

  public getStockList(): Promise<Stock[]> {
    return this.httpService.get("stock", {dateUnix: TimeService.dateNow.toMillis(), symbols: []})
      .pipe(map((info: Stock[]) => this.setCache(info))).toPromise()
      .catch((e) => {
        return this.httpService.getFile('stocks-mock').pipe(map((info: Stock[]) => this.setCache(info))).toPromise()
      });
  }

  public getStock(symbol: string): Promise<Stock> {
    return this.httpService.get("stock/" + symbol, {dateUnix: TimeService.dateNow.toMillis()})
      .pipe(map((info: Stock) => this.updateCache(info))).toPromise();
  }

  public getStockPrices(): Promise<StockPriceSet> {
    return this.httpService.get("stockprices")
      .pipe(map((info: StockPriceSet) => this.setStockPriceSet(info, true))).toPromise()
      ;
  }

  public saveStockPrices(stockPriceSet: StockPriceSet): Promise<StockPriceSet> {
    return this.httpService.post("stockprices", stockPriceSet)
      .pipe(map((info: StockPriceSet) => this.setStockPriceSet(info.prices))).toPromise();
  }

  public processStocks(symbols: string[]) {
    this.httpService.post('stock', symbols).subscribe();
  }

  public updateCacheElement(stock: Stock) {
    const index: number = this.cache.findIndex((stockC: Stock) => stockC.id === stock.id);
    if (index !== -1) {
      const cachedStock: Stock = this.cache[index];
      if (!isEqual(cachedStock, stock)) {
        this.cache.splice(index, 1, stock);
        this.cacheChange.next(stock);
      }
    } else {
      this.cache.push(stock);
      this.cacheChange.next(stock);
    }
    return stock;
  }

  public removeFromCache(id: string) {
    const index: number = this.cache.findIndex(e => e.id === id);
    if (index !== -1) {
      const stock: Stock = this.cache[index];
      this.cache.splice(index, 1);
      this.cacheChange.next(stock);
    }
  }

  public setCache(data: Stock[]): Stock[] {
    this.stockFields = this.keyify(data[0]);
    const quarterFields = Object.keys(data[0].actualQuarter);
    const priceFields = Object.keys(data[0].actualPrice);
    this.quarterFields = quarterFields.filter(f => !f.includes("Data"));
    this.quarterDataFields = quarterFields.filter(f => f.includes("Data"));
    this.quarterDataFieldFields = Object.keys(data[0].actualQuarter?.revenuesData).filter(f => !f.includes("value") && !f.includes("Stats"));
    this.quarterDataFieldsSet = this.quarterDataFieldFields.map(t => upperFirst(t));
    this.priceFields = priceFields.filter(f => !f.includes("Data"));
    this.priceDataFields = priceFields.filter(f => f.includes("Data"));
    this.priceDataFieldFields = Object.keys(data[0].actualPrice?.evData).filter(f => !f.includes("value") && !f.includes("Stats"));
    this.priceDataFieldsSet = this.priceDataFieldFields.map(t => upperFirst(t));
    this.cache = data.map(s => this.mapDataValues(s));
    this.cacheChange.next(null);
    return this.cache;
  }

  public updateCache(stock: Stock) {
    stock = this.mapDataValues(stock);
    this.cache = this.cache.filter(s => s.symbol !== stock.symbol);
    this.cache.push(stock);
    return stock;
  }

  public mapDataValues(s: Stock): Stock {
    s.quarters = s.quarters?.map(q => this.mapQuarterDataValues(q));
    this.mapQuarterDataValues(s.actualQuarter);
    return s;
  }

  public mapQuarterDataValues(q: Quarter): Quarter {
    this.quarterFields.forEach(f => {
      this.quarterDataFieldFields.forEach((qf, i) => {
        if(q[f + "Data"]) {
          q[f + this.quarterDataFieldsSet[i]] =  q[f + "Data"][qf];
        }
      });
    });
    return q;
  }

  public mapPriceDataValues(q: Daily): Daily {
    this.priceFields.forEach(f => {
      this.priceDataFieldFields.forEach((qf, i) => {
        if(q[f + "Data"]) {
          q[f + this.priceDataFieldsSet[i]] =  q[f + "Data"][qf];
        }
      });
    });
    return q;
  }

  public keyify = (obj, prefix = '') => 
      Object.keys(obj).reduce((res, el) => {
        if( Array.isArray(obj[el]) ) {
          return res;
        } else if( typeof obj[el] === 'object' && obj[el] !== null ) {
          return [...res, ...this.keyify(obj[el], prefix + el + '.')];
        }
        return [...res, prefix + el];
  }, []);

  public getFromCache(): Stock[] {
    return JSON.parse(JSON.stringify(this.cache));
  }

  public findBySymbol(symbol: string) {
    return this.cache.find(s => s.symbol === symbol);
  }

  public setDaily(stock: Stock): Observable<Stock> {
    return this.httpService.getFile(stock.symbol).pipe(map((dailyList) => {
      stock.prices = dailyList;
      return stock;
    }));
  }

  public setStockPriceSet(stockPriceSet: StockPriceSet, updateLastPrice?:boolean) {
    this.stockPriceCache = stockPriceSet;
    if (updateLastPrice) {
      this.updateLastPriceCache();
    }
    return this.stockPriceCache;
  }

  public setStockPriceCache(stockPriceList: { [key: string]: number }, updateLastPrice?:boolean) {
    return this.setStockPriceSet({date: TimeService.dateNow.toFormat('dd/MM/yy'), prices: stockPriceList || {}}, updateLastPrice);
  }

  public updateLastPriceCache() {
    const priceFields = Object.keys(this.cache[0].actualPrice).filter(f => f.includes("Data"));
    const priceDataFields = Object.keys(this.cache[0].actualPrice[priceFields[0]]).filter(f => f.includes("Ltm"));
    this.cache.forEach(s => {
      this.calculatePrice(s, s.actualPrice, this.stockPriceCache.prices[s.symbol], priceFields, priceDataFields);
      this.calculateEstPrice(s);
    });
    console.log(this.cache);
  }

  public calculatePrice(s: Stock, priceData: Daily, price: number, priceFields: string[], priceDataFields: string[]) {
    if (price) {
      priceFields.filter(f => priceData[f]?.value).forEach(f => {
        priceData[f]['value'] = getNewValue(priceData[f]['value'], priceData.close, price);
        priceDataFields.filter(df => priceData[f][df]?.value).forEach(df => {
          priceData[f][df]['value'] = getNewValue(priceData[f][df]['value'], priceData.close, price);
        });
      });
      priceData.close = price;
    }
  }

  public calculateEstPrice(stock: Stock) {
    this.calculateQuarterAvgs(stock.actualQuarter);
    this.calculatePriceAvgsAndEst(stock.actualPrice, stock.actualQuarter);
  }

  public calculatePriceAvgsAndEst(price: Daily, quarter: Quarter) {
    const priceFields = Object.keys(price).filter(f => f.includes("Data"));
    const priceDataFields = Object.keys(price[priceFields[0]]).filter(f => f.includes("Ltm"));
    let statsFields = ['lastYStats', 'last3yStats', 'last5yStats'];
    let yearsToProjectList = [1,3,5];
    let statsValues = [];
    let metricGrowthRate: number;
    let ratio: number;
    let actualRatio: number;
    let priceClose: number = price.close;
    let estPrice: any;
    priceFields.forEach(f => {
      priceDataFields.filter(df => !!price[f] && !!price[f][df]).forEach(df => {
        statsValues = statsFields.map(v => getNestedValue(price, `${f}.${df}.${v}.avg`)).filter(v => !!v);
        ratio = mean(statsValues);
        price[f][df]['avg'] = ratio;
        metricGrowthRate = quarter[df.replace('Ltm', 'Data')]['ltmPerShareGrowthAvg'];
        if (metricGrowthRate) {
          actualRatio = price[f][df]['value'];
          yearsToProjectList.forEach(y => {
            estPrice = this.calculateEstPriceField(priceClose, actualRatio, ratio, metricGrowthRate, y);
            price[f][df][y === 1 ? 'estNextY' : `estNext${y}Y`] = estPrice;
          });
        }
      });
    });
    let estPriceList = [];
    let estField;
    let estPriceValue;
    priceFields.filter(f => !!price[f]).forEach(f => {
      yearsToProjectList.forEach(y => {
        estField = y === 1 ? 'estNextY' : `estNext${y}Y`;
        estPriceList = priceDataFields.filter(df => !!price[f][df]).map(df => price[f][df][estField]).filter(v => !!v);
        estPriceValue = mean(estPriceList.map(est => est.futurePrice));
        price[f][estField + 'List'] = estPriceList;
        price[f][estField] = this.calculateFuturePriceGrowth(priceClose, estPriceValue, y);
      });
    });

    yearsToProjectList.forEach(y => {
      estField = y === 1 ? 'estNextY' : `estNext${y}Y`;
      estPriceList = priceFields.filter(f => !!price[f]).map(f => price[f][estField]).filter(v => !!v);
      estPriceValue = mean(estPriceList.map(est => est.futurePrice));
      price[estField] = this.calculateFuturePriceGrowth(priceClose, estPriceValue, y);
    });
  }

  public calculateEstPriceField(priceClose: number, actualRatio: number, ratio: number, metricGrowthRate: number, yearsToProject: number) {
    let fairValue = getNewValue(priceClose, actualRatio, ratio);
    let fairValueGrowth = getPercentage(fairValue, priceClose);
    let futurePrice = applyCagr(fairValue, metricGrowthRate, yearsToProject);
    let futurePriceGrowth = getPercentage(futurePrice, priceClose);
    let cagr = calculateCagr(priceClose, futurePrice, yearsToProject);
    return {
      fairValue,
      fairValueGrowth,
      futurePrice,
      futurePriceGrowth,
      cagr
    }
  }

  public calculateFuturePriceGrowth(priceClose: number, futurePrice: number, yearsToProject: number) {
    let futurePriceGrowth = getPercentage(futurePrice, priceClose);
    let cagr = calculateCagr(priceClose, futurePrice, yearsToProject);
    return {
      futurePrice,
      futurePriceGrowth,
      cagr
    }
  }

  public calculateQuarterAvgs(quarter: Quarter) {
    let statsFields = ['lastYStats', 'last3yStats', 'last5yStats'];
    let statsValues = [];
    this.quarterDataFields.forEach(f => {
      statsValues = statsFields.map(v => getNestedValue(quarter, `${f}.${v}.ltmPerShareGrowthAvg`)).filter(v => !!v);
      quarter[f]['ltmPerShareGrowthAvg'] = mean(statsValues);
    });
  }

  public getActualPrice(symbol: string) {
    return this.stockPriceCache.prices[symbol];
  }

  public getStockFields(): string[] {
    return this.stockFields;
  }
}
