import {Injectable} from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import * as _ from 'lodash';

export enum FunctionCall {
  TIME_SERIES_DAILY = 'TIME_SERIES_DAILY',
  OVERVIEW = 'OVERVIEW'
}

export enum ApiResource {
  API = 'api/v1/'
}

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private httpClient: HttpClient) {
  }

  public getFile(fileName: string): Observable<any> {
    return this.httpClient.get(`assets/${fileName}.json`, { responseType: 'json' });
  }

  public get(url: string, params?: any, resource?: ApiResource): Observable<any> {
    const httpParams = new HttpParams({fromObject: params});
    return this.executeHttpRequest('GET', url, resource, null, httpParams);
  }

  public post(url: string, data: any, resource?: ApiResource): Observable<any> {
    return this.executeHttpRequest('POST', url, resource, data);
  }

  public put(url: string, data: any, resource?: ApiResource): Observable<any> {
    return this.executeHttpRequest('PUT', url, resource, data);
  }

  public delete(url: string, resource?: ApiResource): Observable<any> {
    return this.executeHttpRequest('DELETE', url, resource);
  }

  private executeHttpRequest(method: string, url: string, resource: ApiResource | undefined, body?: any, params?: HttpParams): Observable<any> {
    const headers = this.buildHeaders();
    const resourceUrl = (resource || ApiResource.API).concat(url);
    const options = {headers, body, params};

    return this.httpClient.request(method, resourceUrl, options).pipe(
      catchError((err: HttpErrorResponse) => throwError(console.error(err))),
      map((r: any) => !_.isNil(r.payload) ? r.payload : r));
  }

  private buildHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    });
  }

  public getURL(functionCall: FunctionCall, symbol: string) {
    return `https://www.alphavantage.co/query?function=${functionCall}&symbol=${symbol}&apikey=XLL4HCDO8UHC77AF&outputsize=full`;
  }

  public getFromAPI(functionCall: FunctionCall, symbol: string) {
    return this.httpClient.get(this.getURL(functionCall, symbol));
  }
}
