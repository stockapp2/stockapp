import { ElementRef, Injectable } from '@angular/core';
import * as d3 from 'd3';
import { drag, line, select } from 'd3';
import { compact, first, flatten, flattenDeep, last, max, min, orderBy, uniqBy } from 'lodash';
import { DateTime, Duration } from 'luxon';
import { Observable, Subject } from 'rxjs';
import { getDate, toFormat } from 'src/app/common/luxon-utils';
import { Dimension } from '../models/dimension.model';
import { GraphInfo } from '../models/graph-info.model';
import { GraphPoint } from '../models/graph-point.model';
import { Stock } from '../models/stock.model';
import { getNestedValue } from 'src/app/common/utils';

export interface AttrValue {
  attr: string;
  value: any;
}

export const attrSvg = (a: string, value: any): AttrValue => ({attr: a, value: value});
export const attrCircle = (cx: number, cy: number, r: number): AttrValue[] => [attrSvg('cx', cx), attrSvg('cy', cy), attrSvg('r', r)];

export const addAttrList = (selection: any, list: AttrValue[]): any => {
  list.map(el => selection.attr(el.attr, el.value));
  return selection;
};

@Injectable({
  providedIn: 'root'
})
export class GraphService {

  private maxZoom: number = 8;

  private view: GraphInfo[];
  private fullWidth: number;
  private fullHeight: number;
  private marginLeft: number = 60;
  private marginRight: number = 30;
  private marginTop: number = 0;
  private marginBottom: number = 0;
  private width: number;
  private height: number;
  private focusHeight: number;
  private contextHeight: number = 40;
  private contextScaleHeight: number = 20;
  private topScaleHeight: number = 40;
  private bottomScaleHeight: number = 25;
  private legendHeight: number = 35;
  private mainDiv: any;
  private svgMain: any;
  private gTopScale: any;
  private gLeftScale: any;
  private gFocus: any;
  private gBottomScale: any;
  private gContext: any;
  private gLegend: any;

  private brush: any;
  private zoom: any;
  private xScaleTime: DateTime[];
  private yScaleValue: number[];
  private xScale: any;
  private xScaleContext: any;
  private yScale: any;
  private xAxisTop: any;
  private xAxisBottom: any;
  private yAxistLeft: any;

  private quarterWidth: number;

  constructor() { }

  public drawGraph(stocks: Stock[], quarterFields: string[], dailyFields: string[], dimension: Dimension) {
    this.view = this.getViewFromVisit(stocks, quarterFields, dailyFields);
    this.xScaleTime = this.getXScale();
    this.yScaleValue = this.getYScale();
    if(select('.tooltip').empty()) {
      select('body').append('div').attr('class', 'tooltip');
    }
    this.mainDiv = d3.select('.graph-container');
    this.mainDiv.html('');
    this.fullWidth = dimension.width;
    this.focusHeight = dimension.height;
    this.fullHeight = this.focusHeight + this.topScaleHeight + this.bottomScaleHeight + this.contextHeight + this.marginTop + this.marginBottom + this.legendHeight;
    this.width = this.fullWidth - this.marginLeft - this.marginRight;
    this.height = this.fullHeight - this.marginTop - this.marginBottom;
    this.svgMain = this.mainDiv.append('svg').attr('width', this.fullWidth).attr('height', this.fullHeight + 10);
    this.drawStructure();
    this.drawInfo();
  }

  public drawStructure() {
    this.xScale = d3.scaleTime().range([0, this.width]).domain(d3.extent(this.xScaleTime));
    this.yScale = d3.scaleLinear().range([this.focusHeight, 0,]).domain(d3.extent(this.yScaleValue));
    this.xScaleContext = d3.scaleTime().range([0, this.width]).domain(this.xScale.domain());

    this.xAxisTop = d3.axisTop(this.xScale);
    this.yAxistLeft = d3.axisLeft(this.yScale);

    this.xAxisBottom = d3.axisBottom(this.xScale).tickFormat(d3.timeFormat('%H:%M'));

    this.zoom = d3.zoom()
      .scaleExtent([1, this.maxZoom])
      .translateExtent([[0, 0], [this.width, this.height]])
      .extent([[0, 0], [this.width, this.height]])
      .on('zoom', (event) => this.zoomed(event));

    this.addDefs();

    this.gTopScale = this.svgMain.append('g')
      .attr('class', 'top-scale')
      .attr('transform', 'translate(' + this.marginLeft + ',' + (this.marginTop + this.topScaleHeight - 1) + ')')
      .call(this.xAxisTop);

    this.gLeftScale = this.svgMain.append('g')
      .attr('class', 'left-scale')
      .attr('transform', 'translate(' + this.marginLeft + ',' + (this.marginTop + this.topScaleHeight - 1) + ')')
      .call(this.yAxistLeft);

    this.gFocus = this.svgMain.append('g')
      .attr('class', 'focus')
      .attr('clip-path', 'url(#focus-clip)')
      .attr('transform', 'translate(' + this.marginLeft + ',' + (this.marginTop + this.topScaleHeight) + ')');

    this.gBottomScale = this.svgMain.append('g')
      .attr('class', 'bottom-scale')
      .attr('transform', 'translate(' + this.marginLeft + ',' + (this.marginTop + this.topScaleHeight + this.focusHeight + 1) + ')')
      .call(this.xAxisBottom);

    this.gFocus.append('rect')
      .attr('class', 'zoom')
      .attr('width', this.width)
      .attr('height', this.focusHeight)
      .call(this.zoom);

    this.brush = d3.brushX()
      .extent([[0, 0], [this.width, this.contextHeight - this.contextScaleHeight]])
      .on('brush end', (event) => this.brushed(event));

    this.drawContext();

    
    this.gLegend = this.svgMain.append('g')
      .attr('class', 'legend')
      .attr('transform', 'translate(' + this.marginLeft + ',' + (this.marginTop + this.topScaleHeight + this.focusHeight + this.bottomScaleHeight + this.contextHeight + this.contextScaleHeight) + ')');

    const zoomRange = this.xScaleTime;
    const xRange = [this.xScale(zoomRange[0]), this.xScale(zoomRange[1])];
    const t = d3.zoomIdentity.scale(Math.round(this.width / (xRange[1] - xRange[0]))).translate(-xRange[0], 0);
    this.gFocus.select('.zoom').call(this.zoom.transform, t);
  }

  public drawContext() {
    const xAxisContextBottom = d3.axisBottom(this.xScaleContext).tickFormat(d3.timeFormat('%H:%M'));

    this.gContext = this.svgMain.append('g')
      .attr('class', 'context')
      .attr('transform', 'translate(' + this.marginLeft + ',' + (this.marginTop + this.topScaleHeight + this.focusHeight + this.bottomScaleHeight) + ')');

    this.gContext.append('g')
      .attr('class', 'context-scale')
      .attr('transform', 'translate(0,' + (this.contextHeight - this.contextScaleHeight) + ')')
      .call(xAxisContextBottom);

    this.gContext.append('g')
      .attr('class', 'brush')
      .call(this.brush)
      .call(this.brush.move, this.xScale.range());
  }

  public addDefs() {
    const defs = this.svgMain.append('defs');
    this.addGradient(defs, 'timeline-visit-entry-gradient');
    this.addGradient(defs, 'timeline-visit-exit-gradient');
    defs.append('clipPath').attr('id', 'focus-clip')
      .append('rect')
      .attr('width', this.width)
      .attr('height', this.focusHeight)
      .attr('y', 0);
  }

  public brushed(event) {
    if (event.sourceEvent && (event.mode === 'drag' || event.mode === 'handle')) {
      const s = event.selection || this.xScaleContext.range();
      this.xScale.domain(s.map(this.xScaleContext.invert, this.xScaleContext));
      this.drawInfo();
      this.gTopScale.call(this.xAxisTop);
      this.gBottomScale.call(this.xAxisBottom);
      this.gFocus.select('.zoom').call(this.zoom.transform, d3.zoomIdentity
        .scale(this.width / (s[1] - s[0]))
        .translate(-s[0], 0));
    }
  }

  public zoomed(event) {
    if (event.sourceEvent && event.sourceEvent.type === 'brush') return; // ignore zoom-by-brush
    const t = event.transform;
    this.xScale.domain(t.rescaleX(this.xScaleContext).domain());
    this.gTopScale.call(this.xAxisTop);
    this.gBottomScale.call(this.xAxisBottom);
    this.drawInfo();
    if (this.brush) {
      this.gContext.select('.brush').call(this.brush.move, this.xScale.range().map(t.invertX, t));
    }
  }

  public drawLegendInfo() {
    let gInfo: any;
    let widthRow: any = 150;
    this.gLegend.selectAll('.legend-info').remove();
    this.view.forEach((info: GraphInfo, i: number) => {
      gInfo = this.gLegend.append('g').attr('class', 'legend-info').attr('transform', 'translate(' + (i * widthRow) + ',0)');
      gInfo.append('rect').attr('width', widthRow).attr('height', this.legendHeight).attr('fill', info.color);
      gInfo.append('text').attr('y', this.legendHeight / 2).text(info.field);
    });
  }

  private getXScale(): DateTime[] {
    const dates: DateTime[] = flattenDeep(this.view.map(i => i.points.map(p => p.date)));
    const minDate = min(dates);
    const maxDate = max(dates)?.plus(Duration.fromObject({year: 1}));
    return [minDate, maxDate];
  }

  private getYScale(): number[] {
    const values: number[] = flattenDeep(this.view.map(i => i.points.map(p => p.value)));
    const minValue = min(values);
    const maxValue = max(values);
    return [minValue, maxValue];
  }


  public updateYScale(min: number, max: number): void {
    this.yScaleValue = [min, max];
    this.yScale = d3.scaleLinear().range([this.focusHeight, 0,]).domain(d3.extent(this.yScaleValue));
    this.yAxistLeft = d3.axisLeft(this.yScale);
    this.gLeftScale.call(this.yAxistLeft);
    this.drawInfo();
  }

  public getViewFromVisit(stocks: Stock[], quarterFields: string[], dailyFields: string[]): GraphInfo[] {
    let graphInfo: GraphInfo[] = [];
    let points: GraphPoint[] = [];
    console.log(stocks);
    stocks.forEach(s => {
      quarterFields.forEach(f => {
        points = s.quarters.filter(q => q.fiscalDateEnd).map(q => {
          return {date: getDate(q.fiscalDateEnd), value: q[f]}
        }).filter(p => p.value);
        graphInfo.push({label: s.symbol + ' ' + f, points: points, color: '#' + Math.floor(Math.random()*16777215).toString(16), field: f});
      });
      dailyFields.forEach(f => {
        points = s.prices.filter(q => q.date).map(q => {
          return {date: getDate(q.date), value: getNestedValue(q, f)}
        }).filter(p => p.value);
        graphInfo.push({label: s.symbol + ' ' + f, points: points, color: '#' + Math.floor(Math.random()*16777215).toString(16), field: f});
      });
    });
    console.log(graphInfo);
    return graphInfo;
  }

  public drawInfo() {
    let gInfoGroup;
    let x;
    let y;
    let quartersNext: number;
    this.quarterWidth = Math.abs(this.xScale(DateTime.now()) - this.xScale(DateTime.now().plus(Duration.fromObject({month: 3}))));
    this.gFocus.select('.gInfo').remove();
    let gInfo = this.gFocus.append('g').attr('class', 'gInfo');
    this.view.forEach((info: GraphInfo) => {
      gInfoGroup = gInfo.append('g').attr('id', info.label);
      let str: string = info.points.map(p => {
        x = this.xScale(p.date);
        y = this.yScale(p.value);
        return x + ',' + y + 'L';
      }).join('');
      gInfoGroup.append("path")
      .attr("fill", "none")
      .attr("stroke", info.color)
      .attr("stroke-width", 1.5)
      .attr("d", "M" + str)
      info.points.forEach(p => {
        this.appendCircle(gInfoGroup, p, info.color);
      });
      if(info.field.includes('Next')) {
        quartersNext = this.getQuarterNumberFromText(info.field.substring(info.field.indexOf('Next')).replace('Next', ''));
        gInfoGroup.attr('transform', 'translate(' + (this.quarterWidth * quartersNext)  + ',0)');
      }
    });
    this.drawLegendInfo();
  }

  public appendCircle(g, point: GraphPoint, color: string) {
    let x = this.xScale(point.date);
    let y = this.yScale(point.value);
    if(x && y) {
      this.appendCircleSvg(g, x, y, 2, color)
      .on('mouseover', (e) => this.showTooltipBollard(point, e))
      .on('mouseout', () => this.hideTooltipBollard());
    }
  }

  public appendCircleSvg(g, rx: number, ry: number, r: number, color: string): any {
    return addAttrList(g.append('circle'), attrCircle(rx, ry, r)).attr('fill', color);
  }

  public showTooltipBollard(point: GraphPoint, event: any) {
    let message = toFormat(point.date) + ': ' + point.value;
    select('.tooltip').transition()
      .duration(200)
      .style('opacity', .9);
    select('.tooltip').html(message)
      .style('left', (event.pageX) + 'px')
      .style('top', (event.pageY - 28) + 'px');
  }

  public hideTooltipBollard() {
    select('.tooltip').transition()
      .duration(500)
      .style('opacity', 0);
  }

  public getDimension(element: ElementRef): Dimension {
    return {height: element.nativeElement.offsetHeight, width: element.nativeElement.offsetWidth};
  }

  public getTransformTranslate(str: string): number[] {
    const translate = str.substring(str.indexOf('(') + 1, str.indexOf(')'));
    const tr = translate.split(',');
    const trX = parseFloat(tr[0]);
    const trY = parseFloat(tr[1]);
    return [trX, trY];
  }

  public getTextWidth(textElement: any) {
    return (textElement.node() !== null && textElement.node().getBBox().width !== 0)
      ? textElement.node().getBBox().width
      : textElement.text().length * 5.5;
  }

  public fitTextInRect(textElement: any, text: string, width: number, fromStart?: boolean) {
    const margin = 2;
    let textWidth = this.getTextWidth(textElement);
    width = width - margin;
    while (textWidth > width && text !== '') {
      if (fromStart) {
        text = text.substring(1);
        textElement.text('...' + text);
      } else {
        text = text.slice(0, -1);
        textElement.text(text + '...');
      }
      textWidth = this.getTextWidth(textElement);
    }
    if (text === '') {
      textElement.text('');
    }
  }

  public rotateText(svg: any, dx: string, dy: string, rotate: number, textAnchor: string) {
    svg.selectAll('text')
      .style('text-anchor', textAnchor)
      .attr('dx', dx)
      .attr('dy', dy)
      .attr('transform', `rotate(${rotate})`);
  }

  public getRectPath(x: number, y: number, width: number, height: number, radius: number, first?: boolean, last?: boolean) {
    if (first && last) {
      return this.getRoundedRect(x, y, width, height, radius);
    } else if (first) {
      return this.getLeftRoundedRect(x, y, width, height, radius);
    } else if (last) {
      return this.getRightRoundedRect(x, y, width, height, radius);
    }
    return this.getRect(x, y, width, height);
  }

  public getRightRoundedRect(x: number, y: number, width: number, height: number, radius: number) {
    return 'M' + x + ',' + y +
         'h' + (width - radius) +
         'a' + radius + ',' + radius + ' 0 0 1 ' + radius + ',' + radius +
         'v' + (height - 2 * radius) +
         'a' + radius + ',' + radius + ' 0 0 1 ' + -radius + ',' + radius +
         'h' + (radius - width) +
         'z';
  }

  public getRect(x: number, y: number, width: number, height: number) {
    return 'M' + x + ',' + y +
          'h' + (width) +
          'v' + (height) +
          'h' + (-width) +
          'z';
  }

  public getLeftRoundedRect(x: number, y: number, width: number, height: number, radius: number) {
    return 'M' + (x + radius) + ',' + (y) +
         'h' + (width - radius) +
         'v' + height +
         'h' + -(width - radius) +
         'a' + radius + ',' + radius + ' 0 0 1 ' + -radius + ',' + -radius +
         'v' + (-height + 2 * radius) +
         'a' + radius + ',' + radius + ' 0 0 1 ' + radius + ',' + -radius +
         'z';
  }

  public getRoundedRect(x: number, y: number, width: number, height: number, radius: number) {
    return 'M' + (x + radius) + ',' + (y) +
         'h' + (width - 2 * radius) +
         'a' + radius + ',' + radius + ' 0 0 1 ' + radius + ',' + radius +
         'v' + (height - 2 * radius) +
         'a' + radius + ',' + radius + ' 0 0 1 ' + -radius + ',' + radius +
         'h' + (-width + 2 * radius) +
         'a' + radius + ',' + radius + ' 0 0 1 ' + -radius + ',' + -radius +
         'v' + (-height + 2 * radius) +
         'a' + radius + ',' + radius + ' 0 0 1 ' + radius + ',' + -radius +
         'z';
  }

  public addGradient(defs: any, id: string) {
    const linearGradient = defs.append('linearGradient').attr('id', id);
    linearGradient.attr('x1', '0%').attr('y1', '0%').attr('x2', '100%').attr('y2', '100%');
    linearGradient.append('stop').attr('offset', '0%');
    linearGradient.append('stop').attr('offset', '100%');
  }

  public getQuarterNumberFromText(q: string): number {
    q = q.charAt(0);
    if(q === 'Q') {
      return 1;
    }
    if(q === 'Y') {
      return 4;
    }
    if(q === '3') {
      return 12;
    }
    if(q === '5') {
      return 20;
    }
    return 0;
  }

}

