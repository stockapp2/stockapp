import {Injectable} from '@angular/core';
import { DateTime } from 'luxon';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimeService {

  public static dateFormat = 'dd/MM/yy';
  public static dateNow = DateTime.now();

  private actualDateChange: Subject<DateTime> = new Subject<DateTime>();
  actualDateChange$: Observable<DateTime> = this.actualDateChange.asObservable();

  constructor() {

  }

  public setActualDate(date: DateTime) {
    TimeService.dateNow = date;
    this.actualDateChange.next(date);
  }

}
