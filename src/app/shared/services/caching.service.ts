import { Injectable } from '@angular/core';
import { StockService } from './stock.service';
import { SettingService } from './setting.service';

@Injectable({
  providedIn: 'root'
})
export class CachingService {

  constructor(private stockService: StockService, private settingService: SettingService) { }

  public async initialize(): Promise<boolean> {
    await this.fillCaches();
    return Promise.resolve(true);
  }

  private async fillCaches(): Promise<any> {
    await Promise.all([this.stockService.getStockList(), this.settingService.getSetting()]);
    return Promise.all([this.stockService.getStockPrices()]);
  }

}
