import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculatorProjectedPriceComponent } from './calculator-projected-price.component';

describe('CalculatorProjectedPriceComponent', () => {
  let component: CalculatorProjectedPriceComponent;
  let fixture: ComponentFixture<CalculatorProjectedPriceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CalculatorProjectedPriceComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CalculatorProjectedPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
