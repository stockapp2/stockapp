import { NgClass } from '@angular/common';
import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatCard } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { applyCagr, calculateCagr, getNewValue, getPercentage } from 'src/app/common/math-utils';

@Component({
  selector: 'app-calculator-projected-price',
  standalone: true,
  imports: [MatCard, FormsModule, MatFormFieldModule, MatInputModule, NgClass],
  templateUrl: './calculator-projected-price.component.html',
  styleUrl: './calculator-projected-price.component.scss'
})
export class CalculatorProjectedPriceComponent {

  public price: number = 270;
  public yearsToProject: number = 3;
  public metricGrowthRate: number = 20;
  public priceRatioAvg: number = 33;
  public actualPriceRatio: number = 30;

  public fairValue: number;
  public fairValueGrowth: number;
  public cagr: number;
  public futurePrice: number;
  public futurePriceGrowth: number;

  public updateCalculation() {
    if (this.actualPriceRatio && this.yearsToProject && this.metricGrowthRate) {
      this.fairValue = getNewValue(this.price, this.actualPriceRatio, this.priceRatioAvg);
      this.fairValueGrowth = getPercentage(this.fairValue, this.price);
      this.futurePrice = applyCagr(this.fairValue, this.metricGrowthRate, this.yearsToProject);
      this.futurePriceGrowth = getPercentage(this.futurePrice, this.price);
      this.cagr = calculateCagr(this.price, this.futurePrice, this.yearsToProject);
    }
  }
}
