import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import { StockManagerComponent } from './stock-manager/stock-manager.component';
import { StocksViewComponent } from './stocks-view/stocks-view.component';
import { StockUtilsComponent } from './stock-utils/stock-utils.component';
import { StockDetailComponent } from './stock-detail/stock-detail.component';

const routes: Routes = [
  {path: '', redirectTo: 'stocks-view', pathMatch: 'full'},
  {path: 'stocks-view', component: StocksViewComponent},
  {path: 'stock-admin', component: StockManagerComponent},
  {path: 'stock-utils', component: StockUtilsComponent},
  {path: 'stock-detail/:symbol', component: StockDetailComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
