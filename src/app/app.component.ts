import { Component, OnInit } from '@angular/core';
import { CachingService } from './shared/services/caching.service';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { RouterOutlet } from '@angular/router';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { NgIf } from '@angular/common';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    standalone: true,
    imports: [NgIf, ToolbarComponent, RouterOutlet ]
})
export class AppComponent implements OnInit {

  public cacheComplete: Promise<boolean>;

  constructor(private cachingService: CachingService, private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.matIconRegistry.addSvgIconSetInNamespace(
      'symbol',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/symbol.svg')
    );
    this.cachingService.initialize().then((flag) => { 
      this.cacheComplete = Promise.resolve(flag);
    });
  }
}
