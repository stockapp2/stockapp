import { Component, ElementRef, ViewChild } from '@angular/core';
import { DateTime } from 'luxon';
import { StockService } from '../shared/services/stock.service';
import { FormsModule } from '@angular/forms';
import { MatButton } from '@angular/material/button';
import { CalculatorProjectedPriceComponent } from '../shared/calculator-projected-price/calculator-projected-price.component';
import { CalculatorCagrComponent } from "../shared/calculator-cagr/calculator-cagr.component";

@Component({
    selector: 'app-stock-utils',
    templateUrl: './stock-utils.component.html',
    styleUrls: ['./stock-utils.component.scss'],
    standalone: true,
    imports: [MatButton, FormsModule, CalculatorProjectedPriceComponent, CalculatorCagrComponent]
})
export class StockUtilsComponent {

  public textInput: string;

  @ViewChild('textAreaStock') textAreaStock: ElementRef;

  constructor(private stockService: StockService) {

  }

  public textAreaChange() {
    this.textAreaStock.nativeElement.style.height = '';
    this.textAreaStock.nativeElement.style.height = Math.min(this.textAreaStock.nativeElement.scrollHeight, 600) + 'px';
  }

  public getDates(text: string) {
    const dateRegex = /\b([A-Za-z]{3} \d{2}, \d{4})\b/g;
    const dates: string[] = [];
    let match;
    while ((match = dateRegex.exec(text)) !== null) {
        dates.push(match[1]);
    }

    const luxonDates = dates.map(dateStr => {
        return DateTime.fromFormat(dateStr, 'MMM dd, yyyy');
    });

    const datesFormatted = luxonDates.map(dt => dt.toFormat('M/d/yy')).reverse();
    console.log(datesFormatted.join('\t'));
  }

  public processStocks(text: string) {
    const symbols = text.trim().split(':').filter(s => !!s);
    this.stockService.processStocks(symbols);
  }

  public setActualPrices(text: string) {
    const lines = text.trim().split('\n');

    const symbolPriceMap: { [key: string]: number } = {};
    
    lines.forEach(line => {
        const [symbol, price] = line.split('\t');
        symbolPriceMap[symbol] = this.transformNumber(price);
    });

    const stockPriceSet = this.stockService.setStockPriceCache(symbolPriceMap, true);
    this.stockService.saveStockPrices(stockPriceSet).then();
  }

  transformNumber(numberStr: string): number {
    let cleanStr = numberStr.replace(/,/g, '');
    let num = parseFloat(cleanStr);
    let truncatedNum = Math.floor(num);
    return truncatedNum;
  }

}
