import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockUtilsComponent } from './stock-utils.component';

describe('StockUtilsComponent', () => {
  let component: StockUtilsComponent;
  let fixture: ComponentFixture<StockUtilsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
    imports: [StockUtilsComponent]
});
    fixture = TestBed.createComponent(StockUtilsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
