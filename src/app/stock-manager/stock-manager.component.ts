import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as _ from 'lodash';
import { capitalize, chain, clone, flattenDeep, isEmpty, isNaN, isNumber, last, max, min, round, slice, sum, toNumber, values } from 'lodash';
import { DateTime, Duration } from 'luxon';
import { getDate, toFormat } from '../common/luxon-utils';
import { getPercentage, meanByField, medianByField, stdByField, weightedMeanByField } from '../common/math-utils';
import { avgFields, fieldRanges, fieldRangesDay, fieldRangesDayN, fieldRangesN, futureRanges, futureRangesN } from '../common/utils';
import { Daily } from '../shared/models/price.model';
import { Quarter } from '../shared/models/quarter.model';
import { Stock } from '../shared/models/stock.model';
import { FunctionCall, HttpService } from '../shared/services/http.service';
import { RegexService } from '../shared/services/regex.service';
import { StockService } from '../shared/services/stock.service';
import { TimeService } from '../shared/services/time.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { median } from 'mathjs';
import { NotificationService } from '../shared/services/notification.service';
import { TypeField } from '../shared/models/graph-notification.model';
import { GraphStockComponent } from '../shared/graph-stock/graph-stock.component';
import { FinancialsTableComponent } from '../shared/financials-table/financials-table.component';
import { DropdownFieldsComponent } from '../shared/dropdown-fields/dropdown-fields.component';
import { MatButton } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { MatInput } from '@angular/material/input';

export interface ValueInfo {
  name: string;
  length: number;
}

@Component({
    selector: 'stock-manager',
    templateUrl: './stock-manager.component.html',
    styleUrls: ['./stock-manager.component.scss'],
    standalone: true,
    imports: [MatInput, FormsModule, MatButton, DropdownFieldsComponent, FinancialsTableComponent, GraphStockComponent]
})
export class StockManagerComponent implements OnInit {

  public textInput: string;

  public dailyFieldsSelected: string[];
  public quarterFieldsSelected: string[];

  public stock: Stock;
  public quarters: Quarter[] = [];

  @ViewChild('textAreaStock') textAreaStock: ElementRef;

  constructor(private snackBar: MatSnackBar,
              private notificationService: NotificationService,
              private regexService: RegexService,
              private stockService: StockService,
              private httpService: HttpService) { }

  ngOnInit(): void {

  }

  public textAreaChange() {
    this.textAreaStock.nativeElement.style.height = '';
    this.textAreaStock.nativeElement.style.height = Math.min(this.textAreaStock.nativeElement.scrollHeight, 200) + 'px';
  }

  onDailyFieldsSelectedChange(fields: string[]) {
    this.dailyFieldsSelected = fields;
    this.notificationService.showGraph.next({symbol: this.stock.symbol, fieldsSelected: this.dailyFieldsSelected, typeField: TypeField.DAILY});
  }

  onQuarterFieldsSelectedChange(fields: string[]) {
    this.quarterFieldsSelected = fields;
    this.notificationService.showGraph.next({symbol: this.stock.symbol, fieldsSelected: this.quarterFieldsSelected, typeField: TypeField.QUARTER});
  }

  public onGetStock() {
    this.stock = this.stockService.findBySymbol(this.textInput || 'AMZN');
    if(!this.stock) {
      this.onGetStockFromAPI();
      this.quarters = [];
      this.showMessage('Get from API');
    } else {
      this.quarters = this.stock.quarters;
      this.stock.lastPrice = last(this.stock.prices);
      this.showMessage('Found in cache');
    }
  }

  public onGetStockFromAPI() {
    this.httpService.getFromAPI(FunctionCall.OVERVIEW, this.textInput).subscribe((data: any) => {
      this.stock = {
        symbol: data['Symbol'],
        currency: data['Currency'],
        exchange: data['Exchange'],
        country: data['Country'],
        sector: data['Sector'],
        industry: data['Industry'],
        quarters: []
      };
    });
  }

  public onGetDailyInfo() {
    let priceValues: string[];
    const priceList: Daily[] = this.textInput.split('\n').splice(1).map((text, i) => {
      priceValues = text.split(',');
      return {
        date: DateTime.fromISO(priceValues[0]).toFormat(TimeService.dateFormat),
        open: Number(priceValues[1]),
        high: Number(priceValues[2]),
        low: Number(priceValues[3]),
        close: Number(priceValues[4]),
        adjustedClose: Number(priceValues[5])
      }
    });
    this.stock.prices = priceList;
    this.showMessage('Set daily info');
    this.clearTextArea();
  }

  public sendEarningsDate() {
    let earningDate: DateTime;
    let fiscalDate: DateTime;
    const earningDates = this.regexService.getEarningDates(this.textInput)
      .map(d => getDate(d, 'LLL dd, yyyy'))
      .sort((d1, d2) => d1 < d2 ? -1 : 1);
    if (this.quarters) {
      this.quarters.map((q, i) => {
        fiscalDate = getDate(q.fiscalDateEnd);
        earningDate = chain(earningDates)
          .filter(ed => ed > fiscalDate)
          .first()
          .value();
        if (earningDate && earningDate.diff(fiscalDate, 'months').toObject().months <= 3) {
          q.earningDate = toFormat(earningDate);
        } else {
          q.earningDate = toFormat(fiscalDate.plus(Duration.fromObject({months: 2})));
        }
      });
      this.quarters.push(
        ...earningDates
          .filter(d => d > DateTime.now())
          .map(d => {
            return {earningDate: toFormat(d)};
          }));

      this.stock.quarters = this.quarters;
      this.clearTextArea();
      this.showMessage('Set earnings date');
    }
  }

  public sendIncome() {
    let fiscalPeriodList: string[] = this.getFiscalPeriodList(this.textInput, 'Income Statement');
    let revenues: number[] = this.getNumberList(this.textInput, 'Total Revenues');
    let costsOfGoodsSold: number[] = this.getNumberList(this.textInput, 'Cost of Goods Sold');
    let grossProfit: number[] = this.getNumberList(this.textInput, 'Gross Profit');
    let operatingExpenses: number[] = this.getNumberList(this.textInput, 'Other Operating Expenses');
    let opIncome: number[] = this.getNumberList(this.textInput, 'Operating Income');
    let netIncome: number[] = this.getNumberList(this.textInput, 'Net Income');
    let shares: number[] = this.getNumberList(this.textInput, 'Weighted Average Diluted Shares Outstanding');
  
    this.quarters = fiscalPeriodList.map((d) => {
      return {
        fiscalDateEnd: toFormat(getDate(d, 'L/d/yy'))
      };
    });
    
    const valueInfoList = [
      {name: 'revenues', length: revenues.length}, 
      {name: 'costsOfGoodsSold', length: costsOfGoodsSold.length}, 
      {name: 'grossProfit', length: grossProfit.length}, 
      {name: 'operatingExpenses', length: operatingExpenses.length}, 
      {name: 'opIncome', length: opIncome.length}, 
      {name: 'netIncome', length: netIncome.length}, 
      {name: 'shares', length: shares.length}
    ];

    if (this.checkInfoLenght(valueInfoList)) {
      this.quarters.map((q, i) => {
        q.revenues = revenues[i];
        q.costsOfGoodsSold = costsOfGoodsSold[i];
        q.grossProfit = grossProfit[i];
        q.operatingExpenses = operatingExpenses[i];
        q.operatingIncome = opIncome[i];
        q.netIncome = netIncome[i];
        q.shares = shares[i];
      });

      this.stock.quarters = this.quarters;
      this.clearTextArea();
      this.showMessage('Set income');
    }
  }

  public sendBalance() {
    let totalCashAndShortInv: number[] = this.getNumberList(this.textInput, 'Total Cash And Short Term Investments');
    let totalReceivable: number[] = this.getNumberList(this.textInput, 'Total Receivables');
    let totalCurrentAssets: number[] = this.getNumberList(this.textInput, 'Total Current Assets');
    let netPropertyPlantAndEquip: number[] = this.getNumberList(this.textInput, 'Net Property Plant And Equipment');
    let goodwill: number[] = this.getNumberList(this.textInput, 'Goodwill');
    let totalAssets: number[] = this.getNumberList(this.textInput, 'Total Assets');
    let totalCurrentLiabilities: number[] = this.getNumberList(this.textInput, 'Total Current Liabilities');
    let longTermDebt: number[] = this.getNumberList(this.textInput, 'Long-Term Debt');
    let totalLiabilities: number[] = this.getNumberList(this.textInput, 'Total Liabilities');
    let totalEquity: number[] = this.getNumberList(this.textInput, 'Total Equity');
    let netDebt: number[] = this.getNumberList(this.textInput, 'Net Debt');

    const valueInfoList = [
      {name: 'totalCashAndShortInv', length: totalCashAndShortInv.length}, 
      {name: 'totalReceivable', length: totalReceivable.length}, 
      {name: 'totalCurrentAssets', length: totalCurrentAssets.length}, 
      {name: 'netPropertyPlantAndEquip', length: netPropertyPlantAndEquip.length}, 
      {name: 'goodwill', length: goodwill.length}, 
      {name: 'totalAssets', length: totalAssets.length}, 
      {name: 'totalCurrentLiabilities', length: totalCurrentLiabilities.length}, 
      {name: 'longTermDebt', length: longTermDebt.length}, 
      {name: 'totalLiabilities', length: totalLiabilities.length}, 
      {name: 'totalEquity', length: totalEquity.length},
      {name: 'netDebt', length: netDebt.length}
    ];

    if (this.checkInfoLenght(valueInfoList)) {
      this.quarters.map((q, i) => {
        q.totalCashAndShortInv = totalCashAndShortInv[i];
        q.totalReceivable = totalReceivable[i];
        q.totalCurrentAssets = totalCurrentAssets[i];
        q.netPropertyPlantAndEquip = netPropertyPlantAndEquip[i];
        q.goodwill = goodwill[i];
        q.totalAssets = totalAssets[i];
        q.totalCurrentLiabilities = totalCurrentLiabilities[i];
        q.longTermDebt = longTermDebt[i];
        q.totalLiabilities = totalLiabilities[i];
        q.totalEquity = totalEquity[i];
        q.netDebt = netDebt[i];
      });

      this.stock.quarters = this.quarters;
      this.clearTextArea();
      this.showMessage('Set balance sheet');
    }
  }

  public sendCashFlow() {
    let totalDeprecAmort: number[] = this.getNumberList(this.textInput, 'Total Depreciation & Amortization');
    let stockBasedCompensation: number[] = this.getNumberList(this.textInput, 'Stock-Based Compensation');
    let cashFromOperations: number[] = this.getNumberList(this.textInput, 'Cash from Operations');
    let capitalExpenditure: number[] = this.getNumberList(this.textInput, 'Capital Expenditure');
    let cashFromInvesting: number[] = this.getNumberList(this.textInput, 'Cash from Investing');
    let cashFromFinancing: number[] = this.getNumberList(this.textInput, 'Cash from Financing');
    let netChangeinCash: number[] = this.getNumberList(this.textInput, 'Net Change in Cash');

    const valueInfoList = [
      {name: 'totalDeprecAmort', length: totalDeprecAmort.length}, 
      {name: 'stockBasedCompensation', length: stockBasedCompensation.length}, 
      {name: 'cashFromOperations', length: cashFromOperations.length}, 
      {name: 'capitalExpenditure', length: capitalExpenditure.length}, 
      {name: 'cashFromInvesting', length: cashFromInvesting.length}, 
      {name: 'cashFromFinancing', length: cashFromFinancing.length},
      {name: 'netChangeinCash', length: netChangeinCash.length}
    ];

    if (this.checkInfoLenght(valueInfoList)) {
      this.quarters.map((q, i) => {
        q.totalDeprecAmort = totalDeprecAmort[i];
        q.stockBasedCompensation = stockBasedCompensation[i];
        q.cashFromOperations = cashFromOperations[i];
        q.capitalExpenditure = capitalExpenditure[i];
        q.cashFromInvesting = cashFromInvesting[i];
        q.cashFromFinancing = cashFromFinancing[i];
        q.netChangeinCash = netChangeinCash[i];
      });

      this.stock.quarters = this.quarters;
      this.clearTextArea();
      this.showMessage('Set cash flow');
    }
  }

  public calculateDaily() {
    const quarters = this.stock.quarters;
    let quarterIndex: number = 0;
    let quarter: Quarter = quarters[quarterIndex];
    const firstEarningDate: DateTime = getDate(quarter.earningDate);
    const dailyList = this.stock.prices.filter(d => getDate(d.date) > firstEarningDate);
    let values = [
      'revenues', 'grossProfit', 'operatingIncome', 'netIncome', 
      'freeCashFlow', 'ebitda'
    ];
    let fieldsExt = ['LTM'];
    let dailyRange: Daily[];
    let field: string;
    dailyList.forEach((d, i) => {
      if(quarterIndex < quarters.length - 1 && getDate(d.date) > getDate(quarters[quarterIndex + 1].earningDate)) {
        quarterIndex++;
        quarter = quarters[quarterIndex];
      }
      if(quarter) {
        d.marketCap = quarter.shares * d.close;
        d.ev = d.marketCap + (quarter.netDebt || 0);
        // evRevenuesLTM, mktCapRevenuesLTM, evRevenuesLTMQMedian
        values.forEach(v => {
          fieldsExt.forEach(f => {
            field = 'ev' + capitalize(v) + f;
            d[field] = round(d.ev / quarter[v + f], 2);
            d['mktCap' + capitalize(v) + f] = round(d.marketCap / quarter[v + f], 2);
            fieldRangesDayN.forEach((r, i2) => {
              if (fieldRangesDay[i2] === 'Full') {
                dailyRange = slice(dailyList, 0, i).concat(d);
              } else {
                dailyRange = slice(dailyList, max([i - r, 0]), i).concat(d);
              }
              d = this.calculateAvgField(dailyRange, d, field + fieldRangesDay[i2], field);
/*               futureRangesN.forEach((r2, i3) => {
                q = this.calculateEstField(d, field, fieldRanges[i2], futureRanges[i3], r2);
                q = this.calculateEstField(d, ltmField, fieldRanges[i2], futureRanges[i3], r2);
                q = this.calculateEstField(d, ltmPerShareField, fieldRanges[i2], futureRanges[i3], r2);
              }); */
            });
          });
        });
        d['per'] = round(d.close / quarter['epsLTM'], 2);
        d['fcfYieldEv'] = round(quarter.freeCashFlow / d.ev, 2);
        d['fcfYieldMktCap'] = round(quarter.freeCashFlow / d.marketCap, 2);
      }
    });
    this.stock.prices = dailyList;
    console.timeEnd('Calculate daily');
    this.showMessage('Calculated daily');
  }


  public calculateQuarter() {
    let values = [
      'revenues', 'grossProfit', 'operatingIncome', 'netIncome', 
      'cashFromOperations', 'capitalExpenditure', 'cashFromInvesting', 'cashFromFinancing', 'netChangeinCash',
      'eps', 'freeCashFlow', 'ebitda'
    ];
    console.time('calculateQuarter');
    this.quarters = this.calculateQuarterValues(this.quarters);
    values.forEach(v => this.calculateQuarterValuesByField(this.quarters, v));
    console.timeEnd('calculateQuarter');
    this.stock.quarters = this.quarters;
    this.showMessage('Calculated quarter');
  }

  public calculateQuarterValues(quarters: Quarter[]): Quarter[] {
    let margins = ['grossProfit', 'operatingIncome', 'netIncome', 'ebit', 'ebitda', 'freeCashFlow'];
    quarters.forEach((q, i) => {
      q.eps = round(q.netIncome / q.shares, 2);
      q.freeCashFlow = q.cashFromOperations + q.capitalExpenditure;
      q.ebit = q.grossProfit + q.operatingExpenses;
      q.ebitda = q.operatingIncome + q.totalDeprecAmort;
      q.currentRatio = round(q.totalCurrentAssets / q.totalCurrentLiabilities, 2);
      q.roi = round((q.netIncome / (q.totalAssets - q.totalLiabilities)) * 100, 2);
      margins.forEach((m, i2) => {
        q[m + 'Margin'] = round((q[m] / q.revenues) * 100, 2);
      });
      if(i >= 4) {
        q.roe = round((q.netIncome / ((q.totalEquity + quarters[i - 4].totalEquity) / 2)) * 100, 2);
        q.roa = round(((q.ebit * 0.625) / ((q.totalAssets + quarters[i - 4].totalAssets) / 2)) * 100, 2);
      }
    });
    return quarters;
  }

  public calculateQuarterValuesByField(quarters: Quarter[], field: string): Quarter[] {
    let value: number;
    let shares: number;
    let ltm: number;
    let ltmPerShare: number;
    let growth: number;
    let growthPerShare: number;
    let quartersRange: Quarter[];
    let yearIndex: number = 3;
    let growthField = field + 'Growth';
    let ltmField = field + 'LTM';
    let ltmGrowthField = field + 'LTMGrowth';
    let ltmPerShareField = field + 'LTMPerShare';
    let ltmPerShareGrowthField = field + 'LTMPerShareGrowth';  
    let keys;
    quarters.forEach((q, i) => {
      value = q[field];
      if (value) {
        shares = q['shares'];
        q[field + 'PerShare'] = round(value / shares, 2);
        if (i >= yearIndex) {
          ltm = sum(slice(quarters, i - yearIndex, i).map(q => q[field]).concat(value));
          ltmPerShare = round(ltm / shares, 2);
          q[ltmField] = ltm;
          q[ltmPerShareField] = ltmPerShare, 2;
        }
        if (i >= 4) {
          growth = getPercentage(value, quarters[i - 1][field]);
          q[growthField] = growth;
          growth = getPercentage(ltm, quarters[i - 1][ltmField]);
          q[ltmGrowthField] = growth;
        }
        if (i >= 7) {
          growthPerShare = getPercentage(ltmPerShare, quarters[i - 1][ltmPerShareField]);
          q[ltmPerShareGrowthField] = round(growthPerShare, 2);
        }
        fieldRangesN.forEach((r, i2) => {
          if (fieldRanges[i2] === 'Full') {
            quartersRange = slice(quarters, 0, i).concat(q);
          } else {
            quartersRange = slice(quarters, max([i - r, 0]), i).concat(q);
          }
          q = this.calculateAvgField(quartersRange, q, growthField + fieldRanges[i2], growthField);
          q = this.calculateAvgField(quartersRange, q, ltmGrowthField + fieldRanges[i2], ltmGrowthField);
          q = this.calculateAvgField(quartersRange, q, ltmPerShareGrowthField + fieldRanges[i2], ltmPerShareGrowthField);
          futureRangesN.forEach((r2, i3) => {
            q = this.calculateEstField(q, field, fieldRanges[i2], futureRanges[i3], r2);
            q = this.calculateEstField(q, ltmField, fieldRanges[i2], futureRanges[i3], r2);
            q = this.calculateEstField(q, ltmPerShareField, fieldRanges[i2], futureRanges[i3], r2);
          });
          keys = Object.keys(q);
          futureRangesN.forEach((r2, i3) => {
            q = this.calculateMinMaxMedianEst(q, keys, field, futureRanges[i3]);
            q = this.calculateMinMaxMedianEst(q, keys, ltmField, futureRanges[i3]);
            q = this.calculateMinMaxMedianEst(q, keys, ltmPerShareField, futureRanges[i3]); 
          });
        });
        this.calculateMinMaxMedianGrowth(q, growthField);
        this.calculateMinMaxMedianGrowth(q, ltmGrowthField);
        this.calculateMinMaxMedianGrowth(q, ltmPerShareGrowthField);
      }
    });
    return quarters;
  }

  private calculateMinMaxMedianGrowth(q: Quarter, field: string) {
    let fields = flattenDeep(fieldRanges.map(f => avgFields.map(af => field + f + af)));
    let values = fields.map(f => q[f]);
    this.calculateMinMaxMedian(q, values, field);
    fields = flattenDeep(fieldRanges.map(f => avgFields.map(af => field + f + af + 'Std')));
    values = fields.map(f => q[f]);
    this.calculateMinMaxMedian(q, values, field + 'Std');
  }

  public calculateMinMaxMedianEst(q: Quarter, fields: string[], field: string, futureRange: string): Quarter {
    let values = fields.filter(f => f.includes(field + 'Est') && f.includes('Next' + futureRange)).map(f => q[f]);
    let fieldEst = field + 'Est' + 'Next' + futureRange;
    this.calculateMinMaxMedian(q, values, fieldEst);
    return q;
  }

  public calculateMinMaxMedian(q: Quarter, values: number[], field: string) {
    values = values.filter(v => v);
    if(!isEmpty(values)) {
      q[field + 'Max'] = round(max(values), 2);
      q[field + 'Min'] = round(min(values), 2);
      q[field + 'Median'] = round(median(values), 2);
    }
  }

  public calculateAvgField(quarters: any[], q: any, field: string, fieldAvg: string): any {
    const median = medianByField(quarters, fieldAvg);
    const wMean = weightedMeanByField(quarters, fieldAvg);
    const mean = meanByField(quarters, fieldAvg);
    let fieldA: string;
    let std: number;
    const values = [median, wMean, mean];
    avgFields.forEach((f, i) => {
      fieldA = field + f;
      if (!isNaN(values[i]) && values[i] !== 0) {
        q[fieldA] = values[i];
      }
      std = stdByField(quarters, fieldA);
      if (!isNaN(std) && std !== 0) {
        q[fieldA + 'Std'] = std;
      }
    });
    return q;
  }

  public calculateEstField(q: Quarter, field: string, fieldRange: string, futureFieldRange: string, futureRange: number): Quarter {
    let valueF: number;
    let percentage: number;
    let result: number;
    avgFields.forEach(f => {
      valueF = q[field];
      if (!isNaN(valueF)) {
        percentage = q[field + 'Growth' + fieldRange + f];
        result = round(valueF * Math.pow(1 + percentage / 100, futureRange), 2);
        if(!isNaN(result)) {
          q[field + 'Est' + fieldRange + f + 'Next' + futureFieldRange] = result;
        }
      }
    });
    return q;
  }


  public checkInfoLenght(valueInfoList: ValueInfo[]) {
    const valuesMissing = valueInfoList.filter(e => e.length < this.quarters.length);
    const infoCorrect = valuesMissing.length === 0;
    if (!infoCorrect) {
      this.showMessage('Error in input');
    }
    return infoCorrect;
  }

  public getFiscalPeriodList(text: string, textEnd: string): string[] {
    return this.regexService.getFiscalPeriodEnd(this.getSubs(text, textEnd))
  }

  public getSubs(text: string, startTable: string): string {
    text = text.substring(text.indexOf(startTable));
    return text.substring(0, text.indexOf('\n'));
  }

  public getNumberList(text: string, startTable: string): number[] {
    return this.formatNumber(this.regexService.getNumberList(this.getSubs(text, startTable)));
  }

  public formatNumber(infoList: string[]): number[] {
    return infoList.map(d => {
      d = d.replace('.00', '').replace(',', '');
      return d.includes('(') ? -Number(d.replace('(', '')) : toNumber(d);
    });
  }

  public clearTextArea() {
    this.textInput = '';
    this.textAreaStock.nativeElement.focus();
  }

  public logStock() {
    console.log(this.stock);
  }

  public logDaily() {
    console.log(this.stock.prices);
  }

  public saveStock() {
    this.stock.lastQuarter = last(this.quarters);
    this.stock.lastPrice = last(this.stock.prices);
    if(this.stock.id) {
      
    } else {
      
    }
  }

  public deleteStock() {
    const stock = this.stockService.findBySymbol(this.textInput);
  }

  public showMessage(message: string) {
    this.snackBar.open(message, '', {duration: 1000});
  }

}
