import { compact } from 'lodash';
import * as _ from 'mathjs';
import { ArithmeticType } from './arithmetic-type.enum';


export const mapArrayToField = (array: any[], fieldName: string) => array.map(a => a[fieldName]).filter(v => !!v);


export const round = (number: number, roundDigits?: number) => _.round(number, roundDigits ?? 2);

export const mean = (arrayNumber: number[]) => arrayNumber.length > 0 ? round(_.mean(arrayNumber)) : 0;

export const median = (arrayNumber: number[]) => arrayNumber.length > 0 ? round(_.median(arrayNumber)) : 0;

export const sum = (arrayNumber: number[]) => round(_.sum(arrayNumber));

export const std = (arrayNumber: number[]) => arrayNumber.length > 0 ? round(_.std(arrayNumber as any)) : 0;

export const weightedMean = (arrayNumber: number[]) => {
  arrayNumber = compact(arrayNumber);
  arrayNumber = arrayNumber.map((n, i) => n * (i + 1));
  return round(_.sum(arrayNumber) / getTotalWeight(arrayNumber.length));
};

const getTotalWeight = (n: number) => {
  let result = n;
  for (let num = 0; num < n; num++) {
    result += num;
  }
  return result;
};

export const applyGrowth = (n: number, growth: number) => round(n * (1 + growth / 100));

export const meanByField = (array: any[], fieldName: string) => mean(mapArrayToField(array, fieldName));

export const medianByField = (array: any[], fieldName: string) => median(mapArrayToField(array, fieldName));

export const weightedMeanByField = (array: any[], fieldName: string) => weightedMean(mapArrayToField(array, fieldName));

export const getPercentage = (actualValue: number, previousValue: number) =>
  actualValue && previousValue ? round((actualValue - previousValue) / previousValue * 100) : null;

export const arithmeticMean = (arithmeticType: ArithmeticType, array: any[]) =>
  arithmeticType === ArithmeticType.MEAN ? mean(array) : weightedMean(array);

export const arithmeticMeanByField = (arithmeticType: ArithmeticType, array: any[], fieldName: string) =>
  arithmeticMean(arithmeticType, mapArrayToField(array, fieldName));

export const stdByField = (array: any[], fieldName: string) => std(mapArrayToField(array, fieldName));

export const getNewValue = (oldValue: number, eq: number, newEq: number) => round((oldValue * newEq) / eq);

export const calculateCagr = (initialValue: number, finalValue: number, years: number) => {
  const cagr = Math.pow(finalValue / initialValue, 1 / years) - 1;
  return round(cagr * 100);
}

export const applyCagr = (initialValue: number, cagr: number, years: number): number => {
  const finalValue = initialValue * Math.pow(1 + cagr / 100, years);

  return round(finalValue);
}
