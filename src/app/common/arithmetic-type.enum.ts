export enum ArithmeticType {
  MEAN = 'MEAN',
  WEIGHTED_MEAN = 'WEIGHTED_MEAN'
}
