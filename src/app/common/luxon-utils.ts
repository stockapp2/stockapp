import { DateTime } from "luxon";
import { TimeService } from "../shared/services/time.service";

export const getDate = (date: string, format?: string) => DateTime.fromISO(date);

export const toFormat = (date: DateTime, format?: string) => date?.toFormat(format || TimeService.dateFormat);


