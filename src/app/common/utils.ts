export const avgFields: string[] = ['Median' ,'Wmean', 'Mean'];
export const fieldRanges: string[] = ['Full','5Y', '3Y', 'Y'];
export const fieldRangesN: number[] = [-1, 19, 11, 3];
export const fieldRangesDay: string[] = ['Full','5Y', '3Y', 'Y', 'Q'];
export const fieldRangesDayN: number[] = [-1, 1200, 720, 240, 60];
export const futureRanges: string[] = ['Q','Y', '3Y', '5Y'];
export const futureRangesN: number[] = [1 ,4, 12, 20];

export const getNestedValue = (object: any, fieldPath: string) => {
    return fieldPath.split('.').reduce((obj, key) => obj && obj[key], object);
}