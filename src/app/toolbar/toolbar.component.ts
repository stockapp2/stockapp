import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavigationEnd, Router } from '@angular/router';
import { DateTime } from 'luxon';
import { StockService } from '../shared/services/stock.service';
import { DatepickerComponent } from '../shared/datepicker/datepicker.component';
import { MatOption } from '@angular/material/core';
import { MatAutocompleteTrigger, MatAutocomplete } from '@angular/material/autocomplete';
import { MatInput } from '@angular/material/input';
import { MatFormField } from '@angular/material/form-field';
import { MatButton } from '@angular/material/button';
import { NgIf, NgFor } from '@angular/common';
import { MatToolbar } from '@angular/material/toolbar';
import { TimeService } from '../shared/services/time.service';

@Component({
    selector: 'app-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss'],
    standalone: true,
    imports: [MatToolbar, NgIf, MatButton, FormsModule, MatFormField, MatInput, MatAutocompleteTrigger, ReactiveFormsModule, MatAutocomplete, NgFor, MatOption, DatepickerComponent]
})
export class ToolbarComponent implements OnInit {

  public actualRoute: string;
  myControl = new FormControl('');
  options: string[] = [];
  filteredOptions: string[] = [];
  @ViewChild('input') input: ElementRef<HTMLInputElement>;

  constructor(private router: Router, private stockService: StockService, private timeService: TimeService) {
  }

  ngOnInit(): void {
    this.options = this.stockService.getFromCache().map(s => s.symbol);
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.actualRoute = event.url.substring(1);
      }
    });
    this.timeService.actualDateChange$.subscribe((date: DateTime) => {
      this.onDateChange(date);
    });
  }

  public filter(): void {
    const filterValue = this.input.nativeElement.value.toLowerCase();
    this.filteredOptions = this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  public onOptionClicked(option: string) {
    const valueSelected = this.myControl.value;
    if (valueSelected) {
      this.router.navigate(['/stock-detail', valueSelected]);
      this.clearSearch();
    }
  }

  public clearSearch() {
    this.filteredOptions = this.options;
    this.myControl.setValue('');
  }

  public onDateChange(date: DateTime) {
    this.stockService.getStockList().then();
  }

  public navigate(route: string) {
    this.router.navigate([route]);
  }

}
