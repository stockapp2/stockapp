import { Component, OnInit } from '@angular/core';
import { Stock } from '../shared/models/stock.model';
import { StockService } from '../shared/services/stock.service';
import { MatDialog } from '@angular/material/dialog';
import { ColumnsSetting } from '../shared/models/columns-setting.model';
import { StocksTableComponent } from '../shared/stocks-table/stocks-table.component';
import { MatIcon } from '@angular/material/icon';
import { MatButton } from '@angular/material/button';
import { NgFor } from '@angular/common';
import { FieldSetting } from '../shared/models/field-setting.model';
import { SettingService } from '../shared/services/setting.service';
import { Setting } from '../shared/models/setting.model';
import { SettingSelectorComponent } from "../shared/setting-selector/setting-selector.component";
import { getNestedValue } from '../common/utils';

@Component({
    selector: 'app-stocks-view',
    templateUrl: './stocks-view.component.html',
    styleUrls: ['./stocks-view.component.scss'],
    standalone: true,
    imports: [NgFor, MatButton, MatIcon, StocksTableComponent, SettingSelectorComponent]
})
export class StocksViewComponent implements OnInit {

  fields: string[];
  stocks: Stock[];
  stocksFiltered: Stock[];

  setting: Setting;
  viewList: ColumnsSetting[] = [];
  filterList: ColumnsSetting[] = [];
  viewSelected: ColumnsSetting;
  filterSelected: ColumnsSetting;

  fieldsTable: string[] = [];

  constructor(private stockService: StockService, private settingService: SettingService, public dialog: MatDialog) {}

  ngOnInit() {
    this.initValues();
    this.stockService.cacheChange$.subscribe(() => {
      this.initValues();
    });
  }

  initValues() {
    this.stocks = this.stockService.getFromCache();
    this.stocksFiltered = this.stockService.getFromCache();
    this.setting = this.settingService.getCache();
    this.viewList = this.setting.viewSettings;
    this.filterList = this.setting.filterSettings;
    this.setFieldsTable(this.viewList[0]);
    this.filterTable(this.filterList[0]);
    const stock = this.stocks[0];
    this.fields = Object.keys(stock).concat(stock.actualQuarter ? Object.keys(stock.actualQuarter) : [], Object.keys(stock?.actualPrice));
  }

  setFieldsTable(s: ColumnsSetting) {
    this.viewSelected = s;
    this.fieldsTable = s.fields.map(c => c.field);
  }

  filterTable(s: ColumnsSetting) {
    this.filterSelected = s;
    this.stocksFiltered = this.filterStockList(this.stocks, this.filterSelected?.fields || []);
  }

  filterStockList(objects: Stock[], filters: FieldSetting[]): Stock[] {
    return objects.filter(object => {
      return filters.every(filter => this.applyFilter(object, filter));
    });
  }

  onSaveSettingClick() {
    this.settingService.saveSetting(this.setting);
  }

  private applyFilter(object: Stock, filter: FieldSetting): boolean {
    const value = getNestedValue(object, filter.field);
    switch (filter.comparator) {
      case 'MORE_THAN':
        return value > filter.valueMin;
      case 'LESS_THAN':
        return value < filter.valueMax;
      case 'BETWEEN':
        return value >= filter.valueMin && value <= filter.valueMax;
      case 'EQUAL':
        return value === filter.valueMin;
      default:
        return true;
    }
  }
}
