import { NgFor, NgIf } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatTabGroup, MatTab } from '@angular/material/tabs';
import { ActivatedRoute } from '@angular/router';
import { TypeField } from 'src/app/shared/models/graph-notification.model';
import { Stock } from 'src/app/shared/models/stock.model';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { StockService } from 'src/app/shared/services/stock.service';
import { MatIcon } from '@angular/material/icon';
import { FinancialsTableComponent } from '../shared/financials-table/financials-table.component';
import { GraphStockComponent } from '../shared/graph-stock/graph-stock.component';
import { ExpandableTableComponent } from '../shared/expandable-table/expandable-table.component';
import { TableInfo } from '../shared/models/table-info.model';
import { StatsComponent } from '../shared/stats/stats.component';
import { StatsQuarterComponent } from '../shared/stats-quarter/stats-quarter.component';
import { RowInfo } from '../shared/models/row-info.model';
import { row } from 'mathjs';
import { Quarter } from '../shared/models/quarter.model';
import { avgFields } from '../common/utils';
import { applyGrowth, mean, std } from '../common/math-utils';
import { EstQuarterComponent } from "../shared/est-quarter/est-quarter.component";

@Component({
    selector: 'app-stock-detail',
    templateUrl: './stock-detail.component.html',
    styleUrls: ['./stock-detail.component.scss'],
    standalone: true,
    imports: [MatIcon, NgIf, MatTabGroup, MatTab, FinancialsTableComponent, GraphStockComponent, ExpandableTableComponent, StatsComponent, StatsQuarterComponent, NgFor, EstQuarterComponent]
})
export class StockDetailComponent implements OnInit {
  symbol: string | null = null;
  public stock: Stock;
  public quarterFieldsSelected: string[];
  public priceFieldsSelected: string[];
  public tableInfo: TableInfo;
  public estNextQList = [];

  constructor(private route: ActivatedRoute, private stockService: StockService, private notificationService: NotificationService) {}

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.symbol = params.get('symbol');
      this.stockService.getStock(this.symbol).then((stock) => {
        this.stock = stock;
        console.log(this.stock);
        this.tableInfo = {
          symbol: this.stock.symbol,
          headerInfoList: [{}],
          bodyInfoList: this.mapList()
        } as TableInfo;
        this.estimateNextQuarter(this.stock.actualQuarter);
      });
    });
  }

  public mapList() {
    const fields = Object.keys(this.stock.actualPrice.evData);
    const statsFields = Object.keys(this.stock.actualPrice.evData.revenuesLtm);
    const field = 'evData.';
    let rows: RowInfo[] = [];
    rows.push({field: 'close', values: ['close'], id: 'close', canExpand: false});
    rows.push({field: 'estNextY.futurePrice', values: ['estNextY.futurePrice'], id: 'estNextY.futurePrice', canExpand: false});
    rows.push({field: 'estNext3Y.futurePrice', values: ['estNext3Y.futurePrice'], id: 'estNext3Y.futurePrice', canExpand: false});
    rows.push({field: 'estNext5Y.futurePrice', values: ['estNext5Y.futurePrice'], id: 'estNext5Y.futurePrice', canExpand: false});
    fields.filter(f => !f.includes('estNext')).map((f) => {
      let fieldC = field + f;
      let id = fieldC + '.value';
      rows.push({field: id, values: [fieldC], id: id, canExpand: true});
      statsFields.filter(sf => sf.includes('Stat')).forEach((stField) => {
        let statField = `${fieldC}.${stField}.avg`;
        rows.push({id: statField, field: statField, values: [statField], parentId: id, mainParentId: id, hide: true});
      });
    });
    rows.push({field: 'per', values: ['per']});
    return rows;
  }

  onQuarterFieldsSelectedChange(fields: string[]) {
    console.log(fields);
    this.quarterFieldsSelected = fields;
    this.notificationService.showGraph.next({symbol: this.stock.symbol, fieldsSelected: this.quarterFieldsSelected, typeField: TypeField.QUARTER});
  }

  onPriceFieldsSelectedChange(fields: string[]) {
    console.log(fields);
    this.priceFieldsSelected = fields;
    this.notificationService.showGraph.next({symbol: this.stock.symbol, fieldsSelected: this.priceFieldsSelected, typeField: TypeField.DAILY});
  }

  public estimateNextQuarter(quarter: Quarter) {
    this.estNextQList = ['revenues', 'netIncome', 'eps'].map(f => this.estimateNextQuarterData(quarter, f));
  }

  public estimateNextQuarterData(quarter: Quarter, field: string) {
    const fieldData = field + 'Data';
    const growthValues = this.getValuesField(quarter, fieldData).map(v => v / 4);
    const avgGrowth = mean(growthValues);
    const stdGrowth = std(growthValues);
    const value = quarter[field];
    return {
      value,
      field,
      avgGrowth: avgGrowth * 4,
      stdGrowth: stdGrowth * 4,
      avgEst: applyGrowth(value, avgGrowth),
      lowEst: applyGrowth(value, avgGrowth - stdGrowth),
      highEst: applyGrowth(value, avgGrowth + stdGrowth)
    }
  }

  public getValuesField(quarter: Quarter, fieldData: string): number[] {
    return ['lastYStats', 'last3yStats', 'last5yStats', 'fullStats'].map(s => quarter[fieldData][s]['growthAvg']);
  }




}
