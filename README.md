Stock fields

------------------------------------------------------- Date picker stock list --------------------------------------------------

Common
    - Last quarter = Last quarter from not filtered quarters
    - Last Price = Calculated value from setted price and Last price from not filtered prices 

When date is now (BE)
    - Quarters not filtered
    - Prices not filtered (Add last price calculated??)
    - Actual Quarter = Last Quarter
    - Actual Price = Last Price

When date is before now (BE)
    - Quarters filtered by date
    - Prices filtered by date
    - Actual Quarter = Last quarter from filtered quarters
    - Actual Price = Last price from filtered prices


------------------------------------------------------- Fix token google docs error --------------------------------------------------
Remove from tokens folder the stored credentials file
Whenever you run the service it will require the new authentication and store new credentials file


------------------------------------------------------- Set actual prices -----------------------------------------------------------
From yahoo copy with a format like this

FICO	1,588.52	
GOOGL	185.07	

And it will set the prices for each stock

------------------------------------------------------- Get earnings date -----------------------------------------------------------
From yahoo copy a format like this

IBM	International Business Machines Corporation	Apr 19, 2023, 4 PMEDT	1.26	1.36	+8.1
IBM	International Business Machines Corporation	Jan 25, 2023, 4 PMEST	3.6	3.6	+0.07

It will show a list of dates to be copied in the google docs file